<?php

namespace App\Tests\PlanningV3\Domain\PlanList;

use App\PlanningV3\Domain\Action\ActionData;
use App\PlanningV3\Domain\Action\ActionSymbol;
use App\PlanningV3\Domain\Action\ActionUuid;
use App\PlanningV3\Domain\ImmutablePlan\ListOfActions;
use PHPUnit\Framework\TestCase;

class PlanListTest extends TestCase
{
    /**
     * @throws \App\Common\DomainException
     */
    public function testAsTree()
    {
        $this->markTestSkipped();
        $planList = new ListOfActions();

        $action1 = new ActionData();
        $action1->uuid = ActionUuid::generate();
        $action1->symbol = new ActionSymbol('1');
        $planList->add($action1);

        $action2 = new ActionData();
        $action2->uuid = ActionUuid::generate();
        $action2->symbol = new ActionSymbol('2');
        $planList->add($action2);

        $action1_1 = new ActionData();
        $action1_1->uuid = ActionUuid::generate();
        $action1_1->parentUuid = $action1->uuid;
        $action1_1->symbol = new ActionSymbol('1_1');
        $planList->add($action1_1);

        $action1_1_1 = new ActionData();
        $action1_1_1->uuid = ActionUuid::generate();
        $action1_1_1->parentUuid = $action1_1->uuid;
        $action1_1_1->symbol = new ActionSymbol('1_1_1');
        $planList->add($action1_1_1);

        $action1_1_1_1 = new ActionData();
        $action1_1_1_1->uuid = ActionUuid::generate();
        $action1_1_1_1->parentUuid = $action1_1_1->uuid;
        $action1_1_1_1->symbol = new ActionSymbol('1_1_1_1');
        $planList->add($action1_1_1_1);

        $action1_1_1_2 = new ActionData();
        $action1_1_1_2->uuid = ActionUuid::generate();
        $action1_1_1_2->parentUuid = $action1_1_1->uuid;
        $action1_1_1_2->symbol = new ActionSymbol('1_1_1_2');
        $planList->add($action1_1_1_2);

        $action1_1_2 = new ActionData();
        $action1_1_2->uuid = ActionUuid::generate();
        $action1_1_2->parentUuid = $action1_1->uuid;
        $action1_1_2->symbol = new ActionSymbol('1_1_2');
        $planList->add($action1_1_2);

        $action1_2 = new ActionData();
        $action1_2->uuid = ActionUuid::generate();
        $action1_2->parentUuid = $action1->uuid;
        $action1_2->symbol = new ActionSymbol('1_2');
        $planList->add($action1_2);

        $tree = $planList->asTree();

        echo "\n\n\n";
        $this->printTree($tree);
        echo "\n\n\n";

        $this->assertEquals([], $tree);
    }

    private function printTree($tree, $prefix = '')
    {
        foreach ($tree as $node)
        {
            echo "\n".$prefix.$node['action']['symbol'];
            $this->printTree($node['items'], $prefix.'-');
        }
    }
}

<?php

namespace App\Tests\PlanningV3\Domain\Action;

use App\Common\Amount;
use App\Common\Date;
use App\Common\DomainException;
use App\PlanningV3\Domain\Action\ActionSymbol;
use App\PlanningV3\Domain\Action\Action;
use App\PlanningV3\Domain\Action\ActionType;
use App\PlanningV3\Domain\Action\ActionUuid;
use App\PlanningV3\Domain\Action\BudgetSide;
use PHPUnit\Framework\TestCase;

class ActionTest extends TestCase
{
    private function createAction($symbol = 'DZ/1')
    {
        $uuid = ActionUuid::generate();
        $symbol = new ActionSymbol('DZ/1');
        return Action::create(
            $uuid,
            BudgetSide::EXPENSES(),
            ActionType::TASK(),
            $symbol
        );
    }

    public function testCreate()
    {
        $uuid = ActionUuid::generate();
        $symbol = new ActionSymbol('DZ/1');
        $action = Action::create(
            $uuid,
            BudgetSide::EXPENSES(),
            ActionType::TASK(),
            $symbol
        );

        $this->assertEquals($uuid->toString(), $action->aggregateId());
        $this->assertEquals(BudgetSide::EXPENSES(), $action->side());
        $this->assertEquals(ActionType::TASK(), $action->type());
        $this->assertEquals($symbol, $action->symbol());
    }

    public function testAddingToPlan()
    {
        $action = $this->createAction();
        $plan = Amount::fromPennies(10000);

        $action->increasePlan($plan, Date::fromString('2020-02-06'));

        //test that in history was zero
        $this->assertEquals(Amount::zero(), $action->planAtDate(Date::fromString('2020-02-05')));

        //test current plan
        $this->assertEquals($plan, $action->currentPlan());
        $this->assertEquals($plan, $action->planAtDate(Date::fromString('2020-02-06')));

        //test future plan is same as current
        $this->assertEquals($plan, $action->planAtDate(Date::fromString('2020-02-07')));
        $this->assertEquals($plan, $action->planAtDate(Date::fromString('2020-03-07')));
        $this->assertEquals($plan, $action->planAtDate(Date::fromString('2021-03-07')));
    }

    /**
     * @throws DomainException
     */
    public function testDecreasingPlanToEarly()
    {
        $this->expectException(DomainException::class);

        $action = $this->createAction();
        $action->increasePlan(Amount::fromPennies(10000), Date::fromString('2020-02-05'));

        $action->decreasePlan(Amount::fromPennies(1), Date::fromString('2020-02-04'));
    }

    /**
     * @throws DomainException
     */
    public function testDecreasingPlanToMuchMoney()
    {
        $this->expectException(DomainException::class);

        $action = $this->createAction();
        $action->increasePlan(Amount::fromPennies(10000), Date::fromString('2020-02-05'));
        $action->decreasePlan(Amount::fromPennies(100000), Date::fromString('2020-02-05'));
    }

    /**
     * @throws DomainException
     */
    public function testDecreasingPlanToMuchMoneyV2()
    {
        $this->expectException(DomainException::class);

        $action = $this->createAction();
        $action->increasePlan(Amount::fromPennies(10000), Date::fromString('2020-02-05'));
        $action->decreasePlan(Amount::fromPennies(100000), Date::fromString('2020-02-06'));
    }

    public function testEngagePlan()
    {
        $action = $this->createAction();
        $action->increasePlan(Amount::fromPennies(10000), Date::fromString('2020-02-05'));

        $action->engage(Amount::fromPennies(1000), Date::fromString('2020-02-05'));
        $action->engage(Amount::fromPennies(1000), Date::fromString('2020-02-06'));
        $action->engage(Amount::fromPennies(1000), Date::fromString('2020-02-07'));

        $this->assertEquals(Amount::fromPennies(0), $action->engagedAtDate(Date::fromString('2020-02-04')));
        $this->assertEquals(Amount::fromPennies(1000), $action->engagedAtDate(Date::fromString('2020-02-05')));
        $this->assertEquals(Amount::fromPennies(2000), $action->engagedAtDate(Date::fromString('2020-02-06')));
        $this->assertEquals(Amount::fromPennies(3000), $action->engagedAtDate(Date::fromString('2020-02-07')));
    }

    public function testUnengagePlan()
    {
        $action = $this->createAction();
        $action->increasePlan(Amount::fromPennies(10000), Date::fromString('2020-02-05'));
        $action->engage(Amount::fromPennies(1000), Date::fromString('2020-02-05'));
        $action->engage(Amount::fromPennies(1000), Date::fromString('2020-02-06'));
        $action->engage(Amount::fromPennies(1000), Date::fromString('2020-02-07'));

        $action->unengage(Amount::fromPennies(2000), Date::fromString('2020-02-06'));

        $this->assertEquals(Amount::fromPennies(0), $action->engagedAtDate(Date::fromString('2020-02-04')));
        $this->assertEquals(Amount::fromPennies(1000), $action->engagedAtDate(Date::fromString('2020-02-05')));
        $this->assertEquals(Amount::fromPennies(0), $action->engagedAtDate(Date::fromString('2020-02-06')));
        $this->assertEquals(Amount::fromPennies(1000), $action->engagedAtDate(Date::fromString('2020-02-07')));
    }

    public function testUnengagePlanBeforeEngaged()
    {
        $this->expectException(DomainException::class);

        $action = $this->createAction();
        $action->increasePlan(Amount::fromPennies(10000), Date::fromString('2020-02-05'));
        $action->engage(Amount::fromPennies(1000), Date::fromString('2020-02-05'));
        $action->engage(Amount::fromPennies(1000), Date::fromString('2020-02-06'));
        $action->engage(Amount::fromPennies(1000), Date::fromString('2020-02-07'));

        $action->unengage(Amount::fromPennies(2000), Date::fromString('2020-02-04'));
    }

    public function testUnengagePlanBeforeEngagedV2()
    {
        $action = $this->createAction();
        $action->increasePlan(Amount::fromPennies(10000), Date::fromString('2020-02-05'));
        $action->engage(Amount::fromPennies(1000), Date::fromString('2020-02-05')); //1000
        $action->engage(Amount::fromPennies(1000), Date::fromString('2020-02-06')); //2000
        $action->engage(Amount::fromPennies(1000), Date::fromString('2020-02-07')); //3000

        $action->unengage(Amount::fromPennies(2000), Date::fromString('2020-02-06')); //2000-2000 = 0
    }

    public function testUnengagePlanBeforeEngagedV3()
    {
        $this->expectException(DomainException::class);

        $action = $this->createAction();
        $action->increasePlan(Amount::fromPennies(10000), Date::fromString('2020-01-05'));
        $action->engage(Amount::fromPennies(10000), Date::fromString('2020-03-05'));

        $action->engage(Amount::fromPennies(1), Date::fromString('2020-02-05'));

    }

    public function testEngagePlanBeforeEngagedV4()
    {
        $action = $this->createAction();
        $action->increasePlan(Amount::fromPennies(10000), Date::fromString('2020-01-05'));
        $action->engage(Amount::fromPennies(9999), Date::fromString('2020-03-05'));
        $action->engage(Amount::fromPennies(1), Date::fromString('2020-02-05'));
        $this->assertEquals(Amount::fromPennies(10000), $action->currentEngaged());
    }

}

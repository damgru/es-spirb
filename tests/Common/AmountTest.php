<?php

namespace App\Tests\Common;

use App\Common\Amount;
use PHPUnit\Framework\TestCase;

class AmountTest extends TestCase
{
    public function testPennies()
    {
        $a = Amount::fromPennies(10000);
        $this->assertEquals(10000, $a->inPennies());
    }
}

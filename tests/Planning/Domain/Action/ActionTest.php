<?php

namespace App\Tests\Planning\Domain\Action;
use App\Common\Uuid;
use App\Planning\Domain\Action\Action;
use App\Planning\Domain\Action\ActionStage\ArchiveActionStage;
use App\Planning\Domain\Action\ActionStage\ChangesActionStage;
use App\Planning\Domain\Action\ActionStage\OutgoingActionStage;
use App\Planning\Domain\Action\ActionStage\PrognosisActionStage;
use App\Planning\Domain\Action\ActionStage\ProjectActionStage;
use App\Planning\Domain\Action\ActionStage\PropositionActionStage;
use App\Planning\Domain\Action\ActionType;
use App\Planning\Domain\Action\BudgetSide\Expense;
use App\Planning\Domain\Action\Exception\AllowedAmountExceededException;
use App\Planning\Domain\Action\Exception\UnengageExceededEnagagedAmountException;
use App\Planning\Domain\BudgetYear\BudgetYear;
use Money\Money;
use PHPUnit\Framework\TestCase;

class ActionTest extends TestCase
{
    /** @var Action */
    private $action;

    protected function setUp() : void
    {
        parent::setUp();
        $uuid = Uuid::generate();
        $plan = Money::PLN(0);
        $this->action = Action::create($uuid, $plan, ActionType\ActionTypeTask::typeName(), 'Test');
    }

    public function testCreate()
    {
        $plan = Money::PLN(0);
        $this->assertEquals($plan, $this->action->plan());
    }

    public function testIncreasePlan()
    {
        $money = Money::PLN(10);
        $this->action->increasePlan($money);
        $this->assertEquals($money, $this->action->plan());
    }

    public function testDecreasePlan()
    {
        $money = Money::PLN(10);
        $this->action->increasePlan($money);
        $this->action->decreasePlan($money);
        $this->assertEquals(Money::PLN(0), $this->action->plan());
    }

    public function testCannotDecreasePlanUnderZero()
    {
        $money = Money::PLN(10);
        $this->expectException(AllowedAmountExceededException::class);
        $this->action->decreasePlan($money);
    }

    public function testReserve()
    {
        $money = Money::PLN(70);
        $this->action->increasePlan($money);
        $this->action->reservePlan($money);
        $this->assertEquals($money, $this->action->reserved());
    }

    public function testReserveException()
    {
        $this->expectException(AllowedAmountExceededException::class);
        $this->action->reservePlan(Money::PLN(70));
    }

    public function testAttachToParentAction()
    {
        $parentUuid = '7c53b823-107d-47d5-b249-08f742473ece';
        $this->action->attachToParentAction($parentUuid);
        $this->assertEquals($parentUuid, $this->action->parentUuid());
    }

    public function testEngageMoney()
    {
        $this->action->increasePlan(Money::PLN(100));
        $this->action->engage(Money::PLN(100));
        $this->assertEquals($this->action->plan(), $this->action->engagement());
    }

    public function testEngageToMuchMoney()
    {
        $this->action->increasePlan(Money::PLN(100));

        $this->expectException(AllowedAmountExceededException::class);
        $this->action->engage(Money::PLN(101));
    }

    public function testUnengageMoney()
    {
        $this->action->increasePlan(Money::PLN(100));
        $this->action->engage(Money::PLN(100));

        $this->action->unengage(Money::PLN(50));

        $this->assertEquals(Money::PLN(50), $this->action->engagement());
    }

    public function testUnengageToMuchMoney()
    {
        $this->action->increasePlan(Money::PLN(100));
        $this->action->engage(Money::PLN(100));

        $this->expectException(UnengageExceededEnagagedAmountException::class);
        $this->action->unengage(Money::PLN(101));
    }

    public function testAttachToBudgetYear()
    {
        $budgetYear = BudgetYear::create(Uuid::generate(), 2020);

        $this->action->attachToBudgetYear($budgetYear);

        $this->assertEquals($budgetYear->aggregateId(), $this->action->budgetYearId());
    }

    public function testAttachBudgetSide()
    {
        $side = new Expense();
        $this->action->chooseBudgetSide($side);
        $this->assertEquals($side, $this->action->budgetSide());
    }

    public function testCloseStages()
    {
        $this->assertEquals(PrognosisActionStage::name(), $this->action->currentStage());
        $this->action->close();
        $this->assertEquals(PropositionActionStage::name(), $this->action->currentStage());
        $this->action->close();
        $this->assertEquals(ProjectActionStage::name(), $this->action->currentStage());
        $this->action->close();
        $this->assertEquals(OutgoingActionStage::name(), $this->action->currentStage());
        $this->action->close();
        $this->assertEquals(ChangesActionStage::name(), $this->action->currentStage());
        $this->action->close();
        $this->assertEquals(ArchiveActionStage::name(), $this->action->currentStage());
    }
}

<?php

namespace App\Planning\Command;

use App\Common\Uuid;

final class PlanClose
{
    private $year;

    /**
     * ClosePrognose constructor.
     * @param $year
     */
    public function __construct($year)
    {
        $this->year = $year;
    }


    /**
     * @return int
     */
    public function getYear(): int
    {
        return $this->year;
    }
}
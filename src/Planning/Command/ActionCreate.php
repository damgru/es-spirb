<?php
namespace App\Planning\Command;

use App\Common\Command;
use App\Common\Uuid;
use App\Planning\Domain\Action\ActionType;
use Money\Money;

class ActionCreate implements Command
{
    /** @var Uuid */
    private $actionUuid;
    /** @var Money */
    private $plan;
    /** @var string */
    private $parentUuid;
    /** @var string */
    private $type;
    /** @var string */
    private $symbol;

    public function __construct(Uuid $actionUuid, Money $plan, string $parentUuid, string $typeName, string $symbol)
    {
        $this->actionUuid = $actionUuid;
        $this->plan = $plan;
        $this->parentUuid = $parentUuid;
        $this->type = $typeName;
        $this->symbol = $symbol;
    }

    /**
     * @return Uuid
     */
    public function actionUuid(): Uuid
    {
        return $this->actionUuid;
    }

    /**
     * @return Money
     */
    public function plan(): Money
    {
        return $this->plan;
    }

    /**
     * @return string
     */
    public function parentUuid(): string
    {
        return $this->parentUuid;
    }

    /**
     * @return string
     */
    public function type(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function symbol(): string
    {
        return $this->symbol;
    }


}
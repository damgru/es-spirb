<?php
namespace App\Planning\Command;

use App\Common\Command;
use App\Common\Uuid;

class ActionAttachToParentAction implements Command
{
    /** @var string */
    private $parentUuid;
    /** @var string */
    private $childUuid;

    public function __construct(string $parentUuid, string $childUuid)
    {
        $this->parentUuid = $parentUuid;
        $this->childUuid = $childUuid;
    }

    public function parentUuid(): string
    {
        return $this->parentUuid;
    }

    public function childUuid(): string
    {
        return $this->childUuid;
    }
    
    
    
    
}
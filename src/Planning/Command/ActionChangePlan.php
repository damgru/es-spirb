<?php
namespace App\Planning\Command;

use App\Common\Command;
use App\Common\Uuid;
use Money\Money;

class ActionChangePlan implements Command
{
    /** @var Uuid */
    private $uuid;

    /** @var Money */
    private $amount;

    /**
     * @return Uuid
     */
    public function uuid(): Uuid
    {
        return $this->uuid;
    }

    /**
     * @return Money
     */
    public function amount(): Money
    {
        return $this->amount;
    }

    /**
     * ActionChangePlan constructor.
     * @param Uuid $uuid
     * @param Money $amount
     */
    public function __construct(Uuid $uuid, Money $amount)
    {
        $this->uuid = $uuid;
        $this->amount = $amount;
    }

}
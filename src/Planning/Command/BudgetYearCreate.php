<?php


namespace App\Planning\Command;


use App\Common\Uuid;

class BudgetYearCreate
{
    /** @var Uuid */
    private $uuid;
    /** @var int */
    private $year;

    public function __construct(Uuid $uuid, int $year)
    {
        $this->uuid = $uuid;
        $this->year = $year;
    }

    /**
     * @return Uuid
     */
    public function getUuid(): Uuid
    {
        return $this->uuid;
    }

    /**
     * @return int
     */
    public function getYear(): int
    {
        return $this->year;
    }


}
<?php
namespace App\Planning\Listener;

use App\Common\Aggregate\AggregateChanged;
use App\Common\Aggregate\ApplyByApplyClassShortNameTrait;
use App\Common\Uuid;
use App\Planning\Domain\Action\ActionRepository;
use App\Planning\Domain\Action\Event\ActionAttachedToParentAction;
use App\Planning\Domain\Action\Event\PlanDecreased;
use App\Planning\Domain\Action\Event\PlanIncreased;

class ActionParentListener
{
    use ApplyByApplyClassShortNameTrait;
    /** @var ActionRepository */
    private $repo;

    /**
     * ActionParentListener constructor.
     * @param ActionRepository $repo
     */
    public function __construct(ActionRepository $repo)
    {
        $this->repo = $repo;
    }

    public function __invoke(AggregateChanged $event)
    {
        $this->apply($event);
    }

    public function applyActionAttachedToParentAction(ActionAttachedToParentAction $event)
    {
        $child = $this->repo->getByUuid(Uuid::fromString($event->aggregateId()));
        $parent = $this->repo->getByUuid(Uuid::fromString($event->parentAggregateId()));

        $parent->reservePlan($child->plan());

        $this->repo->save($parent);
    }

    public function applyPlanIncreased(PlanIncreased $event)
    {
        $action = $this->repo->getByUuid(Uuid::fromString($event->aggregateId()));
        $parentUuid = $action->parentUuid();
        if(empty($parentUuid)) return;
        $parentAction = $this->repo->getByUuid(Uuid::fromString($parentUuid));

        $parentAction->reservePlan($event->amount());

        $this->repo->save($parentAction);
    }

    public function applyPlanDecreased(PlanDecreased $event)
    {
        $action = $this->repo->getByUuid(Uuid::fromString($event->aggregateId()));
        $parentUuid = $action->parentUuid();
        if(empty($parentUuid)) return;
        $parentAction = $this->repo->getByUuid(Uuid::fromString($parentUuid));

        $parentAction->unreservePlan($event->amount());

        $this->repo->save($parentAction);
    }
}
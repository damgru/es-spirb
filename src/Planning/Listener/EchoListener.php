<?php
namespace App\Planning\Listener;

use App\Common\Aggregate\AggregateChanged;

class EchoListener
{
    public function __invoke(AggregateChanged $event)
    {
//        echo "###\nECHO LISTENER\n";
//        echo $event->eventName()."\n";
//        echo $event->aggregateId()."\n";
//        echo "\n###\n<br>";
    }
}
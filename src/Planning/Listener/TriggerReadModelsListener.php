<?php


namespace App\Planning\Listener;


use App\Common\Aggregate\AggregateChanged;
use App\Common\ReadModel\ReadModelManager;

class TriggerReadModelsListener
{
    /** @var ReadModelManager */
    private $manager;

    /**
     * TriggerReadModelsListener constructor.
     * @param ReadModelManager $manager
     */
    public function __construct(ReadModelManager $manager)
    {
        $this->manager = $manager;
    }

    public function __invoke(AggregateChanged $event)
    {
        /**
         * @todo we need to consider: this should be triggered by external system like cron every 1 minute or ofter.
         * in other hand: triggering this in client request slows
         */
        $this->manager->trigger();
    }
}
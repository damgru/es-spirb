<?php
namespace App\Planning\CommandHandler;

use App\Common\CommandBus;
use App\Common\Uuid;
use App\Planning\Command\ActionAttachToParentAction;
use App\Planning\Command\ActionCreate;
use App\Planning\Domain\Action\Action;
use App\Planning\Domain\Action\ActionRepository;

class ActionCreateHandler
{
    /** @var ActionRepository */
    private $actionRepo;
    /** @var CommandBus */
    private $commandBus;

    /**
     * ActionService constructor.
     * @param ActionRepository $actionRepo
     * @param CommandBus $commandBus
     */
    public function __construct(ActionRepository $actionRepo, CommandBus $commandBus)
    {
        $this->actionRepo = $actionRepo;
        $this->commandBus = $commandBus;
    }

    public function __invoke(ActionCreate $command)
    {
        $action = Action::create($command->actionUuid(), $command->plan(), $command->type(), $command->symbol());
        $this->actionRepo->save($action);

        $parentUuid = $command->parentUuid();
        if(!empty($parentUuid)) {
            $this->commandBus->handle(new ActionAttachToParentAction($parentUuid, $action->aggregateId()));
        }
    }
}
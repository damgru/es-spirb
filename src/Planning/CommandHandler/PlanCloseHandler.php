<?php

namespace App\Planning\CommandHandler;

use App\Common\ReadModel\ReadModelManager;
use App\Planning\Command\PlanClose;
use App\Planning\CommandHandler\Exception\CommandHandlerException;
use App\Planning\Domain\Action\ActionRepository;
use App\Planning\ReadModel\ActionListView;

final class PlanCloseHandler
{
    /** @var ActionRepository */
    private $actionRepo;

    /** @var ReadModelManager */
    private $readModelManager;

    /**
     * ActionClosePrognoseHandler constructor.
     * @param ActionRepository $actionRepo
     * @param ReadModelManager $readModelManager
     */
    public function __construct(ActionRepository $actionRepo, ReadModelManager $readModelManager)
    {
        $this->actionRepo = $actionRepo;
        $this->readModelManager = $readModelManager;
    }


    public function __invoke(PlanClose $command)
    {
        $year = $command->getYear();
        $view = $this->getUpdatedActionListView();

        $this->validate($view, $year);

        $rows = $view->getRowsForYear($year);
        foreach ($rows as $row) {
            $action = $this->actionRepo->getByUuid($row['uuid']);
            $action->close();
            $this->actionRepo->save($action);
        }
    }

    /**
     * @param ActionListView $view
     * @param int $year
     * @throws CommandHandlerException
     */
    private function validate(ActionListView $view, int $year): void
    {
        $expenses = $view->getExpensesForYear($year);
        $earnings = $view->getEarningsForYear($year);
        if (!$earnings->greaterThanOrEqual($expenses)) {
            throw new CommandHandlerException('');
        }
    }

    /**
     * @return ActionListView
     */
    private function getUpdatedActionListView(): ActionListView
    {
        $this->readModelManager->trigger(ActionListView::class);
        /** @var ActionListView $view */
        $view = $this->readModelManager->getReadModel(ActionListView::class);
        return $view;
    }
}
<?php
namespace App\Planning\CommandHandler;

use App\Common\Uuid;
use App\Planning\Command\ActionAttachToParentAction;
use App\Planning\CommandHandler\Exception\AggregateRootNotFoundException;
use App\Planning\Domain\Action\ActionRepository;

final class ActionAttachToParentActionHandler
{
    /** @var ActionRepository */
    private $actionRepo;

    /**
     * ActionService constructor.
     * @param ActionRepository $actionRepo
     */
    public function __construct(ActionRepository $actionRepo)
    {
        $this->actionRepo = $actionRepo;
    }

    /**
     * @param ActionAttachToParentAction $command
     * @throws AggregateRootNotFoundException
     */
    public function __invoke(ActionAttachToParentAction $command)
    {
        $parent = $this->actionRepo->getByUuid(Uuid::fromString($command->parentUuid()));
        if(empty($parent)) {
            throw new AggregateRootNotFoundException($command->parentUuid());
        }
        $child = $this->actionRepo->getByUuid(Uuid::fromString($command->childUuid()));
        if(empty($parent)) {
            throw new AggregateRootNotFoundException($command->childUuid());
        }

        $child->attachToParentAction($command->parentUuid());

        $this->actionRepo->save($child);
    }
}
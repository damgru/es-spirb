<?php


namespace App\Planning\CommandHandler;


use App\Planning\Command\ActionChangePlan;
use App\Planning\Domain\Action\ActionRepository;
use Money\Money;

class ActionChangePlanHandler
{
    /** @var ActionRepository */
    private $actionRepo;

    public function __invoke(ActionChangePlan $command)
    {
        $action = $this->actionRepo->getByUuid($command->uuid());
        $diff = $command->amount()->subtract($action->plan());

        if ($diff->greaterThan(Money::PLN(0))) {
            $action->increasePlan($diff->absolute());
        }
        elseif ($diff->lessThan(Money::PLN(0))) {
            $action->decreasePlan($diff->absolute());
        }

        $this->actionRepo->save($action);
    }
}
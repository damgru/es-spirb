<?php
namespace App\Planning\CommandHandler\Exception;

class AggregateRootNotFoundException extends CommandHandlerException
{
    /**
     * @var string
     */
    private $aggregateId;

    public function __construct(string $aggregateId)
    {
        $this->aggregateId = $aggregateId;
        parent::__construct("Nie znaleziono pozycji $aggregateId");
    }

    /**
     * @return string
     */
    public function getAggregateId(): string
    {
        return $this->aggregateId;
    }
}
<?php


namespace App\Planning\ReadModel;

use App\Common\Aggregate\AggregateChanged;
use App\Common\Aggregate\ApplyByApplyClassShortNameTrait;
use App\Common\Aggregate\ApplyByClassShortNameTrait;
use App\Common\ReadModel\DbalReadModel;
use App\Common\ReadModel\ReadModel;
use App\Planning\Domain\Action\ActionStage\PrognosisActionStage;
use App\Planning\Domain\Action\ActionType;
use App\Planning\Domain\Action\BudgetSide\Expense;
use App\Planning\Domain\Action\Event\ActionAttachedToBudgetYear;
use App\Planning\Domain\Action\Event\ActionAttachedToParentAction;
use App\Planning\Domain\Action\Event\ActionCreated;
use App\Planning\Domain\Action\Event\ChangeClosed;
use App\Planning\Domain\Action\Event\MoneyEngaged;
use App\Planning\Domain\Action\Event\MoneyUnengaged;
use App\Planning\Domain\Action\Event\OutgoingClosed;
use App\Planning\Domain\Action\Event\PlanDecreased;
use App\Planning\Domain\Action\Event\PlanIncreased;
use App\Planning\Domain\Action\Event\PlanReserved;
use App\Planning\Domain\Action\Event\PlanUnreserved;
use App\Planning\Domain\Action\Event\PrognosisClosed;
use App\Planning\Domain\Action\Event\ProjectClosed;
use App\Planning\Domain\Action\Event\PropositionClosed;
use Money\Money;

class ActionListView extends DbalReadModel implements ReadModel
{
    use ApplyByApplyClassShortNameTrait;

    public function init()
    {
        $sql = <<<SQL
        CREATE TABLE view_action_list (
            uuid uuid PRIMARY KEY,
            parent_uuid uuid,
            symbol text,
            stage text,
            budget_side text,
            year integer,
            year_uuid uuid NULL,
            plan decimal(10,2),
            prognose decimal(10,2),
            proposition decimal(10,2),
            project decimal(10,2),
            outgoing decimal(10,2),
            changes decimal(10,2),
            archive decimal(10,2),
            used decimal(10,2),
            engaged decimal(10,2)
        );
SQL;
        $this->connection->exec($sql);
    }

    public function drop()
    {
        $this->connection->exec("DROP TABLE IF EXISTS view_action_list");
    }

    public function getExpensesForYear($getYear)
    {
        $sql = <<<SQL
        SELECT sum(plan) FROM view_action_list WHERE year = :year AND budget_side = :side
SQL;
        $s = $this->connection->prepare($sql);
        $s->bindValue('year', $getYear);
        $s->bindValue('side', (new Expense())->symbol());
        $s->execute();
        return Money::PLN($s->fetchColumn());
    }

    public function getEarningsForYear($getYear)
    {
        $sql = <<<SQL
        SELECT sum(plan) FROM view_action_list WHERE year = :year AND budget_side = :side
SQL;
        $s = $this->connection->prepare($sql);
        $s->bindValue('year', $getYear);
        $s->bindValue('side', (new Expense())->symbol());
        $s->execute();
        return Money::PLN($s->fetchColumn());
    }

    public function getRowsForYear($getYear)
    {
        $sql = <<<SQL
        SELECT * FROM view_action_list WHERE year = :year
SQL;
        $s = $this->connection->prepare($sql);
        $s->bindValue('year', $getYear);
        $s->execute();
        return $s->fetchAll();
    }

    protected function applyActionCreated(ActionCreated $event)
    {
        $sql = <<<SQL
        INSERT INTO view_action_list VALUES (:uuid,null,:symbol,:stage,'',0,null,0,0,0,0,0,0,0,0,0)
SQL;
        $s = $this->connection->prepare($sql);
        $s->bindValue('uuid', $event->aggregateId());
        $s->bindValue('symbol', $event->symbol());
        $s->bindValue('stage', 'prognose');
        $s->execute();
    }

    protected function applyPlanIncreased(PlanIncreased $event)
    {
        $sql = <<<SQL
        UPDATE view_action_list SET plan = plan + :amount WHERE uuid = :uuid
SQL;
        $s = $this->connection->prepare($sql);
        $s->bindValue('amount', $event->amount()->getAmount());
        $s->bindValue('uuid', $event->aggregateId());
        $s->execute();
    }

    protected function applyPlanDecreased(PlanDecreased $event)
    {
        $sql = <<<SQL
        UPDATE view_action_list SET plan = plan - :amount WHERE uuid = :uuid
SQL;
        $s = $this->connection->prepare($sql);
        $s->bindValue('amount', $event->amount()->getAmount());
        $s->bindValue('uuid', $event->aggregateId());
        $s->execute();
    }

    protected function applyPlanReserved(PlanReserved $event)
    {
        $sql = <<<SQL
        UPDATE view_action_list SET used = used + :amount WHERE uuid = :uuid
SQL;
        $s = $this->connection->prepare($sql);
        $s->bindValue('amount', $event->amount()->getAmount());
        $s->bindValue('uuid', $event->aggregateId());
        $s->execute();
    }

    protected function applyPlanUnreserved(PlanUnreserved $event)
    {
        $sql = <<<SQL
        UPDATE view_action_list SET used = used - :amount WHERE uuid = :uuid
SQL;
        $s = $this->connection->prepare($sql);
        $s->bindValue('amount', $event->amount()->getAmount());
        $s->bindValue('uuid', $event->aggregateId());
        $s->execute();
    }

    protected function applyMoneyEngaged(MoneyEngaged $event)
    {
        $sql = <<<SQL
        UPDATE view_action_list SET engaged = engaged + :amount WHERE uuid = :uuid
SQL;
        $s = $this->connection->prepare($sql);
        $s->bindValue('amount', $event->amount()->getAmount());
        $s->bindValue('uuid', $event->aggregateId());
        $s->execute();
    }

    protected function applyMoneyUnengaged(MoneyUnengaged $event)
    {
        $sql = <<<SQL
        UPDATE view_action_list SET engaged = engaged - :amount WHERE uuid = :uuid
SQL;
        $s = $this->connection->prepare($sql);
        $s->bindValue('amount', $event->amount()->getAmount());
        $s->bindValue('uuid', $event->aggregateId());
        $s->execute();
    }

    protected function applyActionAttachedToParentAction(ActionAttachedToParentAction $event)
    {
        $sql = <<<SQL
        UPDATE view_action_list SET parent_uuid = :parent WHERE uuid = :uuid
SQL;
        $s = $this->connection->prepare($sql);
        $s->bindValue('parent', $event->parentAggregateId());
        $s->bindValue('uuid', $event->aggregateId());
        $s->execute();
    }

    protected function applyPrognosisClosed(PrognosisClosed $event)
    {
        $sql = <<<SQL
        UPDATE view_action_list SET prognose = plan WHERE uuid = :uuid
SQL;
        $s = $this->connection->prepare($sql);
        $s->bindValue('uuid', $event->aggregateId());
        $s->execute();
    }

    protected function applyPropositionClosed(PropositionClosed $event)
    {
        $sql = <<<SQL
        UPDATE view_action_list SET proposition = plan WHERE uuid = :uuid
SQL;
        $s = $this->connection->prepare($sql);
        $s->bindValue('uuid', $event->aggregateId());
        $s->execute();
    }

    protected function applyProjectClosed(ProjectClosed $event)
    {
        $sql = <<<SQL
        UPDATE view_action_list SET project = plan WHERE uuid = :uuid
SQL;
        $s = $this->connection->prepare($sql);
        $s->bindValue('uuid', $event->aggregateId());
        $s->execute();
    }

    protected function applyOutgoingClosed(OutgoingClosed $event)
    {
        $sql = <<<SQL
        UPDATE view_action_list SET project = plan WHERE uuid = :uuid
SQL;
        $s = $this->connection->prepare($sql);
        $s->bindValue('uuid', $event->aggregateId());
        $s->execute();
    }

    protected function applyChangeClosed(ChangeClosed $event)
    {
        $sql = <<<SQL
        UPDATE view_action_list SET project = plan WHERE uuid = :uuid
SQL;
        $s = $this->connection->prepare($sql);
        $s->bindValue('uuid', $event->aggregateId());
        $s->execute();
    }

    protected function applyActionAttachedToBudgetYear(ActionAttachedToBudgetYear $event)
    {
        $sql = <<<SQL
        UPDATE view_action_list SET year = :year, year_uuid = :year_uuid WHERE uuid = :uuid
SQL;
        $s = $this->connection->prepare($sql);
        $s->bindValue('uuid', $event->aggregateId());
        $s->bindValue('year', $event->budgetYear());
        $s->bindValue('year_uuid', $event->budgetYearAggregateId());
        $s->execute();
    }

}
<?php
namespace App\Planning\Domain\BudgetYear;

use App\Common\Uuid;
use App\Planning\Domain\BudgetYear\BudgetYear;

interface BudgetYearRepository
{
    public function getByUuid(Uuid $uuid): BudgetYear;
    public function save(BudgetYear $budgetYear);
}
<?php


namespace App\Planning\Domain\BudgetYear\Event;

use App\Common\Aggregate\AggregateChanged;
use App\Common\Uuid;

class BudgetYearStarted extends AggregateChanged
{
    public static function eventName(): string
    {
        return 'budget-year-started';
    }

    public static function with(Uuid $uuid, int $year)
    {
        return new static($uuid, [
            'year' => $year
        ]);
    }

    public function year(): int
    {
        return $this->payload['year'];
    }

    public function eventDescription(): string
    {
        return 'Rozpoczęto rok budżetowy '.$this->year();
    }
}
<?php


namespace App\Planning\Domain\BudgetYear;


use App\Common\Aggregate\AggregateChanged;
use App\Common\Aggregate\AggregateRoot;
use App\Common\Aggregate\ApplyByApplyClassShortNameTrait;
use App\Common\Uuid;
use App\Planning\Domain\BudgetYear\Event\BudgetYearStarted;
use App\Wallet\Domain\DomainException;

class BudgetYear extends AggregateRoot
{
    use ApplyByApplyClassShortNameTrait;

    /** @var Uuid */
    private $uuid;

    /** @var string */
    private $state;

    /** @var int */
    private $year;

    public static function create(Uuid $uuid, int $year)
    {
        $budgetYear = new self();
        $budgetYear->record(BudgetYearStarted::with($uuid, $year));
        return $budgetYear;
    }

    public function aggregateId(): string
    {
        return $this->uuid->toString();
    }

    public function year(): int
    {
        return $this->year;
    }

    protected function applyBudgetYearStarted(BudgetYearStarted $event)
    {
        $this->uuid = Uuid::fromString($event->aggregateId());
        $this->year = $event->year();
    }
}
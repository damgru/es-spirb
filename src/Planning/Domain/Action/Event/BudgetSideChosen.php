<?php

namespace App\Planning\Domain\Action\Event;

use App\Common\Aggregate\AggregateChanged;
use App\Common\Uuid;
use App\Planning\Domain\Action\BudgetSide\BudgetSide;

final class BudgetSideChosen extends AggregateChanged
{
    public static function eventName(): string
    {
        return 'budget_side_chosen';
    }

    public static function with(string $actionUuid, BudgetSide $budgetSide)
    {
        return new static($actionUuid, [
            'budget_side' => $budgetSide->symbol()
        ]);
    }

    public function budgetSideSymbol(): string
    {
        return $this->payload['budget_side'];
    }

    public function eventDescription(): string
    {
        return 'Przypisano stronę budżetu '.$this->budgetSideSymbol();
    }
}
<?php


namespace App\Planning\Domain\Action\Event;


use App\Common\Aggregate\AggregateChanged;

class ProjectClosed extends AggregateChanged
{

    public static function eventName(): string
    {
        return 'action-project-closed';
    }

    public function eventDescription(): string
    {
        return 'Zamknięto etap projektu';
    }
}
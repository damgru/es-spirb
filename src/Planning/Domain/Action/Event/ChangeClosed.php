<?php
namespace App\Planning\Domain\Action\Event;

use App\Common\Aggregate\AggregateChanged;

class ChangeClosed extends AggregateChanged
{
    public static function eventName(): string
    {
        return 'action-change-closed';
    }

    public function eventDescription(): string
    {
        return 'Zamknięto etap zmian';
    }
}
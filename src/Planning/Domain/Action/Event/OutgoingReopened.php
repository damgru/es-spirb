<?php


namespace App\Planning\Domain\Action\Event;


use App\Common\Aggregate\AggregateChanged;

class OutgoingReopened extends AggregateChanged
{
    public static function eventName(): string
    {
        return 'action-outgoing-reopened';
    }

    public function eventDescription(): string
    {
        return 'Przywrócono etap uchwały';
    }
}
<?php
namespace App\Planning\Domain\Action\Event;

use App\Common\Aggregate\AggregateChanged;

class ChangeReopened extends AggregateChanged
{
    public static function eventName(): string
    {
        return 'action-change-reopened';
    }

    public function eventDescription(): string
    {
        return 'Przywrócono etap zmian';
    }
}
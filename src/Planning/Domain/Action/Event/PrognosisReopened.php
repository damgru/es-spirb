<?php


namespace App\Planning\Domain\Action\Event;


use App\Common\Aggregate\AggregateChanged;

class PrognosisReopened extends AggregateChanged
{
    public static function eventName(): string
    {
        return 'action-prognosis-reopened';
    }

    public function eventDescription(): string
    {
        return 'Przywrócono etap prognozy';
    }
}
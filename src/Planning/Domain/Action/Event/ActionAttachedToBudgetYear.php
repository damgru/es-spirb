<?php

namespace App\Planning\Domain\Action\Event;

use App\Common\Aggregate\AggregateChanged;
use App\Common\Uuid;

final class ActionAttachedToBudgetYear extends AggregateChanged
{
    public static function eventName(): string
    {
        return 'action-attached-to-budget-year';
    }

    public static function with(string $aggregateId, string $budgetYearAggId, int $budgetYear)
    {
        return new static($aggregateId, [
            'budget_year_agg_id' => $budgetYearAggId,
            'budget_year' => $budgetYear
        ]);
    }

    public function budgetYearAggregateId() : string
    {
        return $this->payload['budget_year_agg_id'];
    }

    public function budgetYear() : int
    {
        return (int)$this->payload['budget_year'];
    }

    public function eventDescription(): string
    {
        return 'Przypisano akcję do roku budżetowego '.$this->budgetYear();
    }
}
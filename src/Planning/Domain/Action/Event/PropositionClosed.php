<?php


namespace App\Planning\Domain\Action\Event;


use App\Common\Aggregate\AggregateChanged;

class PropositionClosed extends AggregateChanged
{
    public static function eventName(): string
    {
        return 'action-proposition-closed';
    }

    public function eventDescription(): string
    {
        return 'Zamknięto etap propozycji';
    }
}
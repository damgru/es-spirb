<?php


namespace App\Planning\Domain\Action\Event;


use App\Common\Aggregate\AggregateChanged;

class PropositionReopened extends AggregateChanged
{
    public static function eventName(): string
    {
        return 'action-proposition-reopened';
    }

    public function eventDescription(): string
    {
        return 'Przywrócono etap propozycji';
    }
}
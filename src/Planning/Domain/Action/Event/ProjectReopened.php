<?php


namespace App\Planning\Domain\Action\Event;


use App\Common\Aggregate\AggregateChanged;

class ProjectReopened extends AggregateChanged
{

    public static function eventName(): string
    {
        return 'action-project-reopened';
    }

    public function eventDescription(): string
    {
        return 'Przywrócono etap projektu';
    }
}
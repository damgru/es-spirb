<?php


namespace App\Planning\Domain\Action\Event;


use App\Common\Aggregate\AggregateChanged;

class OutgoingClosed extends AggregateChanged
{
    public static function eventName(): string
    {
        return 'action-outgoing-closed';
    }

    public function eventDescription(): string
    {
        return 'Zamknięto etap uchwały';
    }
}
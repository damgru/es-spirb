<?php
namespace App\Planning\Domain\Action\Event;

use App\Common\Aggregate\AggregateChanged;
use App\Common\Uuid;
use Money\Money;

class MoneyUnengaged extends AggregateChanged
{
    public static function eventName(): string
    {
        return 'action-money-unengaged';
    }

    public static function with(Uuid $uuid, Money $money)
    {
        return new static($uuid->toString(), [
            'amount' => $money->getAmount()
        ]);
    }

    public function amount(): Money
    {
        return Money::PLN($this->payload['amount']);
    }

    public function eventDescription(): string
    {
        return 'Zmniejszono zaangażowanie o '.($this->amount()->getAmount()/100.0).'';
    }
}
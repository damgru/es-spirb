<?php


namespace App\Planning\Domain\Action\Event;


use App\Common\Aggregate\AggregateChanged;

class PrognosisClosed extends AggregateChanged
{
    public static function eventName(): string
    {
        return 'action-prognosis-closed';
    }

    public function eventDescription(): string
    {
        return 'Zamknięto etap prognozy';
    }
}
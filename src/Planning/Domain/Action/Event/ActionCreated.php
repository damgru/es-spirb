<?php


namespace App\Planning\Domain\Action\Event;


use App\Common\Aggregate\AggregateChanged;
use App\Common\Uuid;
use App\Planning\Domain\Action\ActionSymbol;
use App\Planning\Domain\Action\ActionType\ActionType;

class ActionCreated extends AggregateChanged
{
    public static function eventName(): string
    {
        return 'action-created';
    }

    public static function with(Uuid $actionUuid, string $typeName, ActionSymbol $symbol)
    {
        return new static($actionUuid->toString(), [
            'type' => $typeName,
            'symbol' => $symbol->toString()
        ]);
    }

    public function type(): string
    {
        return $this->payload['type'];
    }

    public function symbol(): string
    {
        return $this->payload['symbol'];
    }

    public function eventDescription(): string
    {
        return 'Utworzono akcję '.$this->symbol().' typu '.$this->type();
    }
}
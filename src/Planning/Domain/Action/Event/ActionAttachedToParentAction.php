<?php

namespace App\Planning\Domain\Action\Event;

use App\Common\Aggregate\AggregateChanged;

final class ActionAttachedToParentAction extends AggregateChanged
{
    public static function eventName(): string
    {
        return 'action-attached-to-parent-action';
    }

    public static function with(string $aggregateId, string $parentActionAggid)
    {
        return new static($aggregateId, [
            'parent_agg_id' => $parentActionAggid
        ]);
    }

    public function parentAggregateId() : string
    {
        return $this->payload['parent_agg_id'];
    }

    public function eventDescription(): string
    {
        return 'Przypisano akcję do akcji nadrzędnej '.$this->parentAggregateId();
    }
}
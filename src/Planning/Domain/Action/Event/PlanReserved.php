<?php
namespace App\Planning\Domain\Action\Event;

use App\Common\Aggregate\AggregateChanged;
use App\Common\Uuid;
use Money\Money;

class PlanReserved extends AggregateChanged
{
    public static function eventName(): string
    {
        return 'action-plan-reserved';
    }

    public static function with(Uuid $uuid, Money $money, string $reasonActionUuid = '')
    {
        return new static($uuid->toString(), [
            'amount' => $money->getAmount(),
            'reason_action_uuid' => $reasonActionUuid
        ]);
    }

    public function amount(): Money
    {
        return Money::PLN($this->payload['amount']);
    }

    public function eventDescription(): string
    {
        return 'Zarezerwowano '.($this->amount()->getAmount()/100.0).'';
    }
}
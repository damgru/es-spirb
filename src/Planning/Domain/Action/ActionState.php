<?php

namespace App\Planning\Domain\Action;

use App\Common\Aggregate\ApplyByApplyClassShortNameTrait;
use App\Common\Uuid;
use App\Planning\Domain\Action\ActionStage\ActionStage;
use App\Planning\Domain\Action\ActionStage\ArchiveActionStage;
use App\Planning\Domain\Action\ActionStage\ChangesActionStage;
use App\Planning\Domain\Action\ActionStage\OutgoingActionStage;
use App\Planning\Domain\Action\ActionStage\PrognosisActionStage;
use App\Planning\Domain\Action\ActionStage\ProjectActionStage;
use App\Planning\Domain\Action\ActionStage\PropositionActionStage;
use App\Planning\Domain\Action\ActionType\ActionType;
use App\Planning\Domain\Action\ActionType\ActionTypeAction;
use App\Planning\Domain\Action\ActionType\ActionTypeArticle;
use App\Planning\Domain\Action\ActionType\ActionTypeGroup;
use App\Planning\Domain\Action\ActionType\ActionTypeSection;
use App\Planning\Domain\Action\ActionType\ActionTypeSubaction;
use App\Planning\Domain\Action\ActionType\ActionTypeSubsection;
use App\Planning\Domain\Action\ActionType\ActionTypeSubtask;
use App\Planning\Domain\Action\ActionType\ActionTypeTask;
use App\Planning\Domain\Action\BudgetSide\BudgetSide;
use App\Planning\Domain\Action\BudgetSide\BudgetSideFactory;
use App\Planning\Domain\Action\Event\ActionAttachedToBudgetYear;
use App\Planning\Domain\Action\Event\ActionAttachedToParentAction;
use App\Planning\Domain\Action\Event\ActionCreated;
use App\Planning\Domain\Action\Event\BudgetSideChosen;
use App\Planning\Domain\Action\Event\ChangeClosed;
use App\Planning\Domain\Action\Event\MoneyEngaged;
use App\Planning\Domain\Action\Event\MoneyUnengaged;
use App\Planning\Domain\Action\Event\OutgoingClosed;
use App\Planning\Domain\Action\Event\PlanDecreased;
use App\Planning\Domain\Action\Event\PlanIncreased;
use App\Planning\Domain\Action\Event\PlanReserved;
use App\Planning\Domain\Action\Event\PlanUnreserved;
use App\Planning\Domain\Action\Event\PrognosisClosed;
use App\Planning\Domain\Action\Event\ProjectClosed;
use App\Planning\Domain\Action\Event\PropositionClosed;
use Money\Money;

final class ActionState
{
    use ApplyByApplyClassShortNameTrait;
    
    /** @var Uuid */
    public $uuid;
    /** @var Money */
    public $plan;
    /** @var Money */
    public $reserved;
    /** @var Money */
    public $engagement;
    /** @var string */
    public $parentUuid = '';
    /** @var ActionStage */
    public $stage;
    /** @var ActionType */
    public $type;
    /** @var string */
    public $budgetYearId;
    /** @var BudgetSide */
    public $budgetSide;

    protected function applyBudgetSideChosen(BudgetSideChosen $event)
    {
        $this->budgetSide = BudgetSideFactory::fromSymbol($event->budgetSideSymbol());
    }
    
    protected function applyActionCreated(ActionCreated $event)
    {
        $this->uuid = Uuid::fromString($event->aggregateId());
        $this->plan = Money::PLN(0);
        $this->reserved = Money::PLN(0);
        $this->engagement = Money::PLN(0);
        $this->stage = new PrognosisActionStage();

        switch ($event->type()) {
            case ActionTypeSection::typeName(): $this->type = new ActionTypeSection(); break;
            case ActionTypeSubsection::typeName(): $this->type = new ActionTypeSubsection(); break;
            case ActionTypeTask::typeName(): $this->type = new ActionTypeTask(); break;
            case ActionTypeSubtask::typeName(): $this->type = new ActionTypeSubtask(); break;
            case ActionTypeAction::typeName(): $this->type = new ActionTypeAction(); break;
            case ActionTypeSubaction::typeName(): $this->type = new ActionTypeSubaction(); break;
            case ActionTypeGroup::typeName(): $this->type = new ActionTypeGroup(); break;
            case ActionTypeArticle::typeName(): $this->type = new ActionTypeArticle(); break;
        }
    }

    protected function applyActionAttachedToParentAction(ActionAttachedToParentAction $event)
    {
        $this->parentUuid = $event->parentAggregateId();
    }

    protected function applyActionAttachedToBudgetYear(ActionAttachedToBudgetYear $event)
    {
        $this->budgetYearId = $event->budgetYearAggregateId();
    }

    protected function applyPrognosisClosed(PrognosisClosed $event)
    {
        $this->stage = new PropositionActionStage();
    }

    protected function applyPropositionClosed(PropositionClosed $event)
    {
        $this->stage = new ProjectActionStage();
    }

    protected function applyProjectClosed(ProjectClosed $event)
    {
        $this->stage = new OutgoingActionStage();
    }

    protected function applyOutgoingClosed(OutgoingClosed $event)
    {
        $this->stage = new ChangesActionStage();
    }

    protected function applyChangeClosed(ChangeClosed $event)
    {
        $this->stage = new ArchiveActionStage();
    }

    protected function applyPlanIncreased(PlanIncreased $event)
    {
        $this->plan = $this->plan->add($event->amount());
    }

    protected function applyPlanDecreased(PlanDecreased $event)
    {
        $this->plan = $this->plan->subtract($event->amount());
    }

    protected function applyPlanReserved(PlanReserved $event)
    {
        $this->reserved = $this->reserved->add($event->amount());
    }

    protected function applyPlanUnreserved(PlanUnreserved $event)
    {
        $this->reserved = $this->reserved->subtract($event->amount());
    }

    protected function applyMoneyEngaged(MoneyEngaged $event)
    {
        $this->engagement = $this->engagement->add($event->amount());
    }

    protected function applyMoneyUnengaged(MoneyUnengaged $event)
    {
        $this->engagement = $this->engagement->subtract($event->amount());
    }
}
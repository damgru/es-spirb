<?php

namespace App\Planning\Domain\Action;

use App\Common\Aggregate\AggregateChanged;
use App\Common\Aggregate\AggregateRoot;
use App\Common\Aggregate\ApplyByApplyClassShortNameTrait;
use App\Common\Uuid;
use App\Planning\Domain\Action\ActionStage\ArchiveActionStage;
use App\Planning\Domain\Action\BudgetSide\BudgetSide;
use App\Planning\Domain\Action\Event\ActionAttachedToBudgetYear;
use App\Planning\Domain\Action\Event\ActionAttachedToParentAction;
use App\Planning\Domain\Action\Event\ActionCreated;
use App\Planning\Domain\Action\Event\BudgetSideChosen;
use App\Planning\Domain\Action\Event\MoneyEngaged;
use App\Planning\Domain\Action\Event\MoneyUnengaged;
use App\Planning\Domain\Action\Event\PlanDecreased;
use App\Planning\Domain\Action\Event\PlanIncreased;
use App\Planning\Domain\Action\Event\PlanUnreserved;
use App\Planning\Domain\Action\Event\PlanReserved;
use App\Planning\Domain\Action\Exception\AllowedAmountExceededException;
use App\Planning\Domain\Action\Exception\CannotMakeChangesInArchiveAction;
use App\Planning\Domain\Action\Exception\UnengageExceededEnagagedAmountException;
use App\Planning\Domain\BudgetYear\BudgetYear;
use Money\Money;

class Action extends AggregateRoot
{
    use ApplyByApplyClassShortNameTrait;
    /** @var ActionState */
    private $state;

    /**
     * @param Uuid $uuid
     * @param Money $plan
     * @param string $typeName
     * @param string $symbol
     * @return Action
     * @throws \App\Common\DomainException
     */
    public static function create(Uuid $uuid, Money $plan, string $typeName, string $symbol)
    {
        $action = new self();
        $symbol = new ActionSymbol($symbol);
        $action->record(ActionCreated::with($uuid, $typeName, $symbol));
        $action->increasePlan($plan);
        return $action;
    }

    /**
     * @param string $parentUuid
     * @return void
     */
    public function attachToParentAction(string $parentUuid)
    {
        $this->record(ActionAttachedToParentAction::with($this->aggregateId(), $parentUuid));
    }

    public function attachToBudgetYear(BudgetYear $budgetYear)
    {
        $this->record(ActionAttachedToBudgetYear::with($this->aggregateId(), $budgetYear->aggregateId(), $budgetYear->year()));
    }

    public function chooseBudgetSide(BudgetSide $budgetSide)
    {
        $this->record(BudgetSideChosen::with($this->aggregateId(), $budgetSide));
    }

    /**
     * @throws \App\Common\DomainException
     */
    public function close()
    {
        $this->state->stage->validateClose($this);
        foreach ($this->state->stage->closeEvent($this->aggregateId()) as $event) {
            $this->record($event);
        }
    }

    public function reopen()
    {
        $this->state->stage->validateReopen($this);
        foreach ($this->state->stage->reopenEvent($this->aggregateId()) as $event) {
            $this->record($event);
        }
    }

    public function increasePlan(Money $money)
    {
        if ($this->state->stage instanceof ArchiveActionStage) {
            throw new CannotMakeChangesInArchiveAction();
        }

        $this->record(PlanIncreased::with($this->state->uuid, $money));
    }

    /**
     * @param Money $money
     * @throws AllowedAmountExceededException
     * @throws \App\Common\DomainException
     */
    public function decreasePlan(Money $money)
    {
        if ($this->state->stage instanceof ArchiveActionStage) {
            throw new CannotMakeChangesInArchiveAction();
        }

        if ($this->state->plan->subtract($money)->lessThan($this->state->reserved->add($this->state->engagement)))
        {
            throw new AllowedAmountExceededException();
        }

        $this->record(PlanDecreased::with($this->state->uuid, $money));
    }

    /**
     * @param Money $money
     * @throws AllowedAmountExceededException
     * @throws CannotMakeChangesInArchiveAction
     * @throws \App\Common\DomainException
     */
    public function reservePlan(Money $money)
    {
        if ($this->state->stage instanceof ArchiveActionStage) {
            throw new CannotMakeChangesInArchiveAction();
        }

        if ($this->state->plan->lessThan($this->reserved()->add($money)))
        {
            throw new AllowedAmountExceededException([
                'plannedAmount' => $this->state->plan->getAmount(),
                'planUsed' => $this->state->reserved->getAmount(),
                'engagement' => $this->state->engagement->getAmount(),
                'moneyToReserve' => $money->getAmount()
            ]);
        }

        $this->record(PlanReserved::with($this->state->uuid, $money));
    }

    public function unreservePlan(Money $money)
    {
        if ($this->state->stage instanceof ArchiveActionStage) {
            throw new CannotMakeChangesInArchiveAction();
        }

        $this->record(PlanUnreserved::with($this->state->uuid, $money));
    }

    public function engage(Money $money)
    {
        if ($this->state->stage instanceof ArchiveActionStage) {
            throw new CannotMakeChangesInArchiveAction();
        }

        if ($this->state->plan->lessThan($this->reserved()->add($money)))
        {
            throw new AllowedAmountExceededException();
        }

        $this->record(MoneyEngaged::with($this->state->uuid, $money));
    }

    public function unengage(Money $money)
    {
        if ($this->state->stage instanceof ArchiveActionStage) {
            throw new CannotMakeChangesInArchiveAction();
        }

        if($this->state->plan->lessThan($money)) {
            throw new UnengageExceededEnagagedAmountException();
        }

        $this->record(MoneyUnengaged::with($this->state->uuid, $money));
    }

    public function plan() : Money
    {
        return $this->state->plan;
    }

    /**
     * @return string
     */
    public function aggregateId(): string
    {
        return $this->state->uuid->toString();
    }

    public function parentUuid(): string
    {
        return $this->state->parentUuid;
    }

    public function budgetYearId(): string
    {
        return $this->state->budgetYearId;
    }

    public function reserved() : Money
    {
        return $this->state->reserved;
    }

    public function engagement() : Money
    {
        return $this->state->engagement;
    }

    public function currentStage()
    {
        return $this->state->stage::name();
    }

    public function budgetSide() : BudgetSide
    {
        return $this->state->budgetSide;
    }

    public function apply(\App\Common\Aggregate\AggregateChanged $event): void
    {
        if(empty($this->state)) {
            $this->state = new ActionState();
        }

        $this->state->apply($event);
    }


}
<?php
namespace App\Planning\Domain\Action;

use App\Common\Uuid;
use App\Planning\Domain\Action\Action;

interface ActionRepository
{
    public function getByUuid(Uuid $uuid): Action;
    public function save(Action $action);
}
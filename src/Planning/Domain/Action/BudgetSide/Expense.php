<?php

namespace App\Planning\Domain\Action\BudgetSide;

final class Expense implements BudgetSide
{
    public function symbol(): string
    {
        return 'W';
    }
}
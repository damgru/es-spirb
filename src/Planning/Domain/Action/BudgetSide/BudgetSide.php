<?php

namespace App\Planning\Domain\Action\BudgetSide;

interface BudgetSide
{
    public function symbol() : string ;
}
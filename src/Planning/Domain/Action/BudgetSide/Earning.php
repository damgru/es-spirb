<?php

namespace App\Planning\Domain\Action\BudgetSide;

final class Earning implements BudgetSide
{
    public function symbol(): string
    {
        return 'D';
    }
}
<?php

namespace App\Planning\Domain\Action\BudgetSide;

final class BudgetSideFactory
{
    public static function fromSymbol($symbol) : BudgetSide
    {
        if ($symbol === 'D') {
            return new Earning();
        }
        elseif ($symbol === 'W') {
            return new Expense();
        }
    }
}
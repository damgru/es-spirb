<?php
namespace App\Planning\Domain\Action\ActionStage;

use App\Common\Aggregate\AggregateChanged;
use App\Planning\Domain\Action\Action;
use App\Planning\Domain\Action\Event\PrognosisClosed;

class PrognosisActionStage implements ActionStage
{
    public function validateClose(Action $action): bool
    {
        return true;
    }

    public function closeEvent(string $aggregateId): iterable
    {
        yield new PrognosisClosed($aggregateId);
    }

    public static function name(): string
    {
        return 'prognosis';
    }

    public function validateReopen(Action $param)
    {
        // TODO: Implement validateReopen() method.
    }

    public function reopenEvent(string $aggregateId): iterable
    {
        // TODO: Implement reopenEvent() method.
    }
}
<?php
namespace App\Planning\Domain\Action\ActionStage;

use App\Common\Aggregate\AggregateChanged;
use App\Planning\Domain\Action\Action;
use App\Planning\Domain\Action\Event\PrognosisReopened;
use App\Planning\Domain\Action\Event\PropositionClosed;

class PropositionActionStage implements ActionStage
{
    public function validateClose(Action $action): bool
    {
        return true;
    }

    public function closeEvent(string $aggregateId): iterable
    {
        yield new PropositionClosed($aggregateId);
    }

    public static function name(): string
    {
        return 'proposition';
    }

    public function reopenEvent(string $aggregateId): iterable
    {
        yield new PrognosisReopened($aggregateId);
    }

    public function validateReopen(Action $param)
    {
        // TODO: Implement validateReopen() method.
    }
}
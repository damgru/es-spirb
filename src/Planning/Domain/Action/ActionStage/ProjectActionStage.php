<?php


namespace App\Planning\Domain\Action\ActionStage;


use App\Common\Aggregate\AggregateChanged;
use App\Common\DomainException;
use App\Planning\Domain\Action\Action;
use App\Planning\Domain\Action\Event\ProjectClosed;
use App\Planning\Domain\Action\Event\PropositionReopened;

class ProjectActionStage implements ActionStage
{
    /**
     * @param Action $action
     * @return void
     * @throws DomainException
     */
    public function validateClose(Action $action)
    {

    }

    public function closeEvent(string $aggregateId): iterable
    {
        yield new ProjectClosed($aggregateId);
    }

    public static function name(): string
    {
        return 'project';
    }

    public function validateReopen(Action $param)
    {
        // TODO: Implement validateReopen() method.
    }

    public function reopenEvent(string $aggregateId): iterable
    {
        yield new PropositionReopened($aggregateId);
    }
}
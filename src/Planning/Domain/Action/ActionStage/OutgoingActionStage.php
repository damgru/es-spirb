<?php
namespace App\Planning\Domain\Action\ActionStage;

use App\Common\DomainException;
use App\Planning\Domain\Action\Action;
use App\Planning\Domain\Action\Event\OutgoingClosed;
use App\Planning\Domain\Action\Event\ProjectReopened;

class OutgoingActionStage implements ActionStage
{
    /**
     * @param Action $action
     * @return void
     * @throws DomainException
     */
    public function validateClose(Action $action)
    {

    }

    public function closeEvent(string $aggregateId): iterable
    {
        yield new OutgoingClosed($aggregateId);
    }

    public static function name(): string
    {
        return 'outgoing';
    }

    public function reopenEvent(string $aggregateId): iterable
    {
        yield new ProjectReopened($aggregateId);
    }

    public function validateReopen(Action $param)
    {

    }
}
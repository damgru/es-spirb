<?php
namespace App\Planning\Domain\Action\ActionStage;

use App\Common\Aggregate\AggregateChanged;
use App\Planning\Domain\Action\Action;
use App\Planning\Domain\Action\Event\ChangeClosed;
use App\Planning\Domain\Action\Event\ChangeReopened;
use App\Planning\Domain\Action\Event\OutgoingReopened;

class ChangesActionStage implements ActionStage
{
    public function validateClose(Action $action)
    {

    }

    public function closeEvent(string $aggregateId): iterable
    {
        yield new ChangeClosed($aggregateId);
    }

    public static function name(): string
    {
        return 'changes';
    }

    public function reopenEvent(string $aggregateId): iterable
    {
        yield new OutgoingReopened($aggregateId);
    }

    public function validateReopen(Action $param)
    {

    }
}
<?php
namespace App\Planning\Domain\Action\ActionStage;

use App\Common\Aggregate\AggregateChanged;
use App\Planning\Domain\Action\Action;
use App\Planning\Domain\Action\Event\ChangeReopened;
use App\Planning\Domain\Action\Exception\CannotCloseArchiveActionException;

class ArchiveActionStage implements ActionStage
{
    public function validateClose(Action $action)
    {
        throw new CannotCloseArchiveActionException();
    }

    public function closeEvent(string $aggregateId): iterable
    {
        throw new CannotCloseArchiveActionException();
    }

    public function reopenEvent(string $aggregateId): iterable
    {
        yield new ChangeReopened($aggregateId);
    }

    public static function name(): string
    {
        return 'archive';
    }

    public function validateReopen(Action $param)
    {

    }
}
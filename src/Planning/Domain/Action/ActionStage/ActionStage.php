<?php
namespace App\Planning\Domain\Action\ActionStage;

use App\Common\Aggregate\AggregateChanged;
use App\Planning\Domain\Action\Action;

interface ActionStage
{
    public function validateClose(Action $action);
    public function closeEvent(string $aggregateId): iterable;

    public function validateReopen(Action $param);
    public function reopenEvent(string $aggregateId): iterable;

    public static function name(): string;
}
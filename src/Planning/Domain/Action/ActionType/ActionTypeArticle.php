<?php

namespace App\Planning\Domain\Action\ActionType;

final class ActionTypeArticle implements ActionType
{
    static function typeName(): string
    {
        return 'article';
    }
}
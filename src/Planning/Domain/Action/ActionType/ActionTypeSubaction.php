<?php

namespace App\Planning\Domain\Action\ActionType;

final class ActionTypeSubaction implements ActionType
{
    static function typeName(): string
    {
        return 'subaction';
    }
}
<?php

namespace App\Planning\Domain\Action\ActionType;

final class ActionTypeSection implements ActionType
{
    static function typeName(): string
    {
        return 'section';
    }
}
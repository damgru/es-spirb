<?php

namespace App\Planning\Domain\Action\ActionType;

interface ActionType
{
    static function typeName() : string;
}
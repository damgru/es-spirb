<?php

namespace App\Planning\Domain\Action\ActionType;

final class ActionTypeGroup implements ActionType
{
    static function typeName(): string
    {
        return 'group';
    }
}
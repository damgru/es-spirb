<?php

namespace App\Planning\Domain\Action\ActionType;

final class ActionTypeTask implements ActionType
{
    static function typeName(): string
    {
        return 'task';
    }
}
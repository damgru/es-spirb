<?php

namespace App\Planning\Domain\Action\ActionType;

final class ActionTypeAction implements ActionType
{
    static function typeName(): string
    {
        return 'action';
    }
}
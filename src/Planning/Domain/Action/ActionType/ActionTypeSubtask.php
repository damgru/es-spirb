<?php

namespace App\Planning\Domain\Action\ActionType;

final class ActionTypeSubtask implements ActionType
{
    static function typeName(): string
    {
        return 'subtask';
    }
}
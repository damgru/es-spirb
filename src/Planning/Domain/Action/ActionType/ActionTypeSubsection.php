<?php

namespace App\Planning\Domain\Action\ActionType;

final class ActionTypeSubsection implements ActionType
{
    static function typeName(): string
    {
        return 'subsection';
    }
}
<?php

namespace App\Planning\Domain\Action\Exception;

use App\Common\DomainException;

final class CannotMakeChangesInArchiveAction extends DomainException
{
    public function __construct()
    {
        parent::__construct("Nie można wprowadzać zmian do akcji, która jest już w stanie archiwalnym");
    }
}
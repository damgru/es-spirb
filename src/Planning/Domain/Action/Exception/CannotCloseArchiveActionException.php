<?php


namespace App\Planning\Domain\Action\Exception;


use App\Common\DomainException;

class CannotCloseArchiveActionException extends DomainException
{
    public function __construct()
    {
        parent::__construct("Nie można zamknąć akcji, która jest już w stanie archiwalnym");
    }
}
<?php

namespace App\Planning\Domain\Action\Exception;

use App\Common\DomainException;
use Throwable;

class AllowedAmountExceededException extends DomainException
{
    public function __construct(array $debugData = [], Throwable $previous = null)
    {
        parent::__construct("Przekroczono kwotę planu", $debugData, $previous);
    }

}
<?php

namespace App\Planning\Domain\Action\Exception;

use App\Common\DomainException;

final class UnengageExceededEnagagedAmountException extends DomainException
{
    public function __construct()
    {
        parent::__construct("Zaaangażowana kwota jest mniejsza niż kwota wyangażowania");
    }
}
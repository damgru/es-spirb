<?php

namespace App\Planning\Domain\Action\Exception;

use App\Common\DomainException;

final class CannotReopenActionInPrognosisStage extends DomainException
{
    public function __construct()
    {
        parent::__construct("Nie można wycofać akcji, która jest już na etapie prognozy");
    }
}
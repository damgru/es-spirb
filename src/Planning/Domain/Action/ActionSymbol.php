<?php
namespace App\Planning\Domain\Action;

class ActionSymbol
{
    private $symbol;

    /**
     * ActionSymbol constructor.
     * @param $symbol
     */
    public function __construct(string $symbol)
    {
        $this->symbol = $symbol;
    }

    /**
     * @return mixed
     */
    public function toString()
    {
        return $this->symbol;
    }


}
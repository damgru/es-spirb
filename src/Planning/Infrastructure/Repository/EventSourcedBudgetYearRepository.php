<?php
namespace App\Planning\Infrastructure\Repository;

use App\Common\Aggregate\AggregateRepository;
use App\Common\Uuid;
use App\Planning\Domain\Action\Action;
use App\Planning\Domain\Action\ActionRepository;
use App\Planning\Domain\BudgetYear\BudgetYear;
use App\Planning\Domain\BudgetYear\BudgetYearRepository;

class EventSourcedBudgetYearRepository extends AggregateRepository implements BudgetYearRepository
{
    public function getByUuid(Uuid $uuid): BudgetYear
    {
        return $this->loadAggregateRoot(Action::class, $uuid->toString());
    }

    public function save(BudgetYear $budgetYear)
    {
        $this->saveAggregateRoot($budgetYear);
    }
}
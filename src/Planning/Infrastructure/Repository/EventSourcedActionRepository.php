<?php
namespace App\Planning\Infrastructure\Repository;

use App\Common\Aggregate\AggregateRepository;
use App\Common\Uuid;
use App\Planning\Domain\Action\Action;
use App\Planning\Domain\Action\ActionRepository;

class EventSourcedActionRepository extends AggregateRepository implements ActionRepository
{
    public function getByUuid(Uuid $uuid): Action
    {
        return $this->loadAggregateRoot(Action::class, $uuid->toString());
    }

    public function save(Action $action)
    {
        $this->saveAggregateRoot($action);
    }

}
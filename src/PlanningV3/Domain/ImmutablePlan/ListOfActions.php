<?php

namespace App\PlanningV3\Domain\ImmutablePlan;

use App\PlanningV3\Domain\Action\ActionData;

final class ListOfActions
{
    /** @var ActionData[] */
    private array $actions = [];
    private array $tree = [];
    private array $tmp = [];

    public static function fromActions(ActionData ...$actions) : ListOfActions
    {
        $list = new ListOfActions();
        $list->actions = $actions;
        return $list;
    }

    public function add(ActionData $action)
    {
        $this->actions[$action->uuid->toString()] = $action;
        $this->tree = [];
    }

    public function asArray()
    {
        $array = [];
        $array[] = $this->actions->asArray();
    }

    public function asTree()
    {
        if (!empty($this->tree)) {
            return $this->tree;
        }

        $this->tree = [];
        $this->tmp = [];
        foreach ($this->actions as $action)
        {
            $id = $action->uuid->toString();
            if ($action->isRoot()) {
                $this->tree[$id] = [
                    'action' => $action->asArray(),
                    'items'  => []
                ];
                $this->tmp[$id] = &$this->tree[$id];
            } else {
                $parentId = $action->parentUuid->toString();
                if (!empty($this->tmp[$parentId])) {
                    $this->tmp[$parentId]['items'][$id] = [
                        'action' => $action->asArray(),
                        'items' => []
                    ];
                    $this->tmp[$id] = &$this->tmp[$parentId]['items'][$id];
                }
            }
        }
        $this->tree = $tree;
        return $tree;
    }
}
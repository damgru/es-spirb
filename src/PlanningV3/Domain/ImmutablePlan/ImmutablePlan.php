<?php

namespace App\PlanningV3\Domain\ImmutablePlan;

use App\Common\Uuid;
use App\PlanningV3\Domain\ImmutablePlan\PlanStatus;

final class ImmutablePlan
{
    private int $id;
    private ImmutablePlanUuid $uuid;
    private \DateTimeImmutable $atDate;
    private int $version;
    private int $year;
    private PlanStatus $status;
    private ListOfActions $expenses;
    private ListOfActions $earnings;
    private ListOfActions $income;
    private ListOfActions $outgo;

    private \DateTimeImmutable $created;

    /**
     * ImmutablePlan constructor.
     * @param ImmutablePlanUuid $uuid
     * @param int $version
     * @param int $year
     * @param \DateTimeImmutable $atDate
     * @param ListOfActions $expenses
     * @param ListOfActions $earnings
     * @param ListOfActions $income
     * @param ListOfActions $outgo
     */
    public function __construct(ImmutablePlanUuid $uuid, int $version, int $year, \DateTimeImmutable $atDate, ListOfActions $expenses, ListOfActions $earnings, ListOfActions $income, ListOfActions $outgo)
    {
        $this->uuid = $uuid;
        $this->version = $version;
        $this->year = $year;
        $this->atDate = $atDate;
        $this->expenses = $expenses;
        $this->earnings = $earnings;
        $this->income = $income;
        $this->outgo = $outgo;
    }

    /** @return int */
    public function id()
    {
        return $this->id;
    }

    /** @return ImmutablePlanUuid */
    public function uuid()
    {
        return $this->uuid;
    }

    /** @return mixed */
    public function version()
    {
        return $this->version;
    }

    /** @return mixed */
    public function year()
    {
        return $this->year;
    }

    /** @return PlanStatus */
    public function status(): PlanStatus
    {
        return $this->status;
    }

    /** @return ListOfActions */
    public function expenses(): ListOfActions
    {
        return $this->expenses;
    }

    /** @return ListOfActions */
    public function earnings(): ListOfActions
    {
        return $this->earnings;
    }

    /** @return ListOfActions */
    public function income(): ListOfActions
    {
        return $this->income;
    }

    /** @return ListOfActions */
    public function outgo(): ListOfActions
    {
        return $this->outgo;
    }
}
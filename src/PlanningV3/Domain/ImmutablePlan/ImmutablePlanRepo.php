<?php

namespace App\PlanningV3\Domain\ImmutablePlan;

interface ImmutablePlanRepo
{
    public function save(ImmutablePlan $plan);
    public function getByUuid(ImmutablePlanUuid $uuid) : ImmutablePlan;
}
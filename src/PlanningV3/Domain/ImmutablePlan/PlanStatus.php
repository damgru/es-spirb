<?php

namespace App\PlanningV3\Domain\ImmutablePlan;

use App\Common\Enum;

/**
 * Class PlanStatus
 * @package App\PlanningV3\Domain\ImmutablePlan
 * @method static PlanStatus TEMPORARY()
 * @method static PlanStatus CONFIRMED()
 * @method static PlanStatus REVOKED()
 */
final class PlanStatus extends Enum
{
    const TEMPORARY = 'T';
    const CONFIRMED = 'C';
    const REVOKED   = 'R';
}
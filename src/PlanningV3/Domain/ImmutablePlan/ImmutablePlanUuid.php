<?php

namespace App\PlanningV3\Domain\ImmutablePlan;

use App\Common\UuidTrait;

final class ImmutablePlanUuid
{
    use UuidTrait;
}
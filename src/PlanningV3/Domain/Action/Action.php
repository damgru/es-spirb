<?php

namespace App\PlanningV3\Domain\Action;

use App\Common\Aggregate\AggregateChanged;
use App\Common\Aggregate\AggregateRoot;
use App\Common\Aggregate\ApplyByApplyClassShortNameTrait;
use App\Common\Amount;
use App\Common\Date;
use App\Common\DomainException;
use App\Common\Uuid;

final class Action extends AggregateRoot
{
    private ActionData $state;

    public function __construct()
    {
        $this->state = new ActionData();
    }

    public static function create(ActionUuid $uuid, BudgetSide $budgetSide, ActionType $typeName, ActionSymbol $symbol)
    {
        $action = new self();
        $action->record(Event\ActionCreated::with($uuid->toString(), $budgetSide->getValue(), $typeName->getValue(), $symbol->toString()));
        return $action;
    }

    public function aggregateId(): string
    {
        return $this->state->uuid->toString();
    }

    public function isRoot(): bool
    {
        return $this->state->isRoot();
    }

    public function asData(): ActionData
    {
        return $this->state->copy();
    }

    public function side(): BudgetSide
    {
        return $this->state->side;
    }

    public function type(): ActionType
    {
        return $this->state->type;
    }

    public function symbol(): ActionSymbol
    {
        return $this->state->symbol;
    }

    public function currentPlan(): Amount
    {
        return $this->state->plan();
    }

    public function planAtDate(Date $date): Amount
    {
        return $this->state->planToDate($date);
    }

    public function currentEngaged(): Amount
    {
        return $this->state->engaged();
    }

    public function engagedAtDate(Date $date): Amount
    {
        return $this->state->engagedToDate($date);
    }

    public function increasePlan(Amount $amount, Date $atDate)
    {
        $this->record(Event\ActionPlanIncreased::with($this->aggregateId(), $amount, $atDate));
    }

    public function decreasePlan(Amount $amount, Date $atDate)
    {
        $plan = $this->state->planToDate($atDate);
        $engage = $this->state->engagedToDate($atDate);
        if ($plan->subtract($amount)->lessThan($engage)) {
           throw new DomainException('Plan przekracza wykorzystane środki na dzień');
        }

        $plan = $this->state->plan();
        $engage = $this->state->engaged();
        if ($plan->subtract($amount)->lessThan($engage)) {
            throw new DomainException('Plan przekracza aktualnie wykorzystane środki');
        }

        $this->record(Event\ActionPlanDecreased::with($this->aggregateId(), $amount, $atDate));
    }

    public function engage(Amount $amount, Date $atDate)
    {
        $plan = $this->state->planToDate($atDate);
        $engage = $this->state->engagedToDate($atDate);
        if ($plan->subtract($engage)->lessThan($amount)) {
            throw new DomainException('suma zaangazowań jest większa niż kwota planu na dzień');
        }

        $plan = $this->state->plan();
        $engage = $this->state->engaged();
        if ($plan->subtract($engage)->lessThan($amount)) {
            throw new DomainException('suma zaangazowań jest większa niż aktualna kwota planu');
        }

        $this->record(Event\ActionEngaged::with($this->aggregateId(), $amount, $atDate));
    }

    public function unengage(Amount $amount, Date $atDate)
    {
        $engageToDate = $this->state->engagedToDate($atDate);
        if ($engageToDate->lessThan($amount)) {
            throw new DomainException('suma zaangazowań na dzień jest mniejsza niż kwota zmniejszenia zaangażowania');
        }
        $engage = $this->state->engaged();
        if ($engage->lessThan($amount)) {
            throw new DomainException('aktualna suma zaangazowań jest mniejsza niż kwota zmniejszenia zaangażowania');
        }

        $this->record(Event\ActionUnengaged::with($this->aggregateId(), $amount, $atDate));
    }

    public function apply(AggregateChanged $event): void
    {
        $this->state->apply($event);
        $this->state->actionVersion = $this->version();
    }
}
<?php

namespace App\PlanningV3\Domain\Action;

use App\Common\Enum;

final class BudgetSide extends Enum
{
    const EXPENSES = 'W';
    const EARNINGS = 'D';
    const INCOME = 'P';
    const OUTGOING = 'R';
}
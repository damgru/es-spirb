<?php

namespace App\PlanningV3\Domain\Action;

use App\Common\Aggregate\ApplyByApplyClassShortNameTrait;
use App\Common\Amount;
use App\Common\Date;
use DeepCopy\DeepCopy;

final class ActionData
{
    use ApplyByApplyClassShortNameTrait;
    
    public ActionUuid $uuid;
    public int $actionVersion = 0;

    public BudgetSide $side;
    public ActionType $type;

    public ?ActionUuid $parentUuid;
    
    public array $related = [];

    public ActionSymbol $symbol;

    /** @var Amount[] */
    public array $plan;
    /** @var Amount[] */
    public array $engaged;

    /**
     * ActionData constructor.
     */
    public function __construct()
    {
        $this->engaged = [];
        $this->plan = [];
    }


    public function copy() : ActionData {
        $copier = new DeepCopy(true);
        return $copier->copy($this);
    }

    public function asArray() : array
    {
        return [
            'uuid'          => $this->uuid->toString(),
            'parent_uuid'   => empty($this->parentUuid) ? null : $this->parentUuid->toString(),
            'version'       => $this->actionVersion,
            'type'          => empty($this->type) ? null : $this->type->getValue(),
            'side'          => empty($this->side) ? null : $this->side->getValue(),
            'related'       => empty($this->related) ? null : $this->related,
            'symbol'        => empty($this->symbol) ? null : $this->symbol->toString(),
            'plan'          => $this->plan()->inPennies(),
            'engaged'       => $this->engaged()->inPennies(),
        ];
    }

    public function plan() : Amount
    {
        if (empty($this->plan)) {
           return Amount::zero();
        }
        return Amount::sum(...array_values($this->plan));
    }

    public function planToDate(Date $atDate) : Amount
    {
        $sum = Amount::zero();
        foreach ($this->plan as $day => $amount)
        {
            if (Date::fromString($day)->isBeforeOrEqual($atDate)) {
                $sum = $sum->add($amount);
            }
        }
        return $sum;
    }

    public function engagedToDate(Date $atDate) : Amount
    {
        $sum = Amount::zero();
        foreach ($this->engaged as $day => $amount)
        {
            if (Date::fromString($day)->isBeforeOrEqual($atDate)) {
                $sum = $sum->add($amount);
            }
        }
        return $sum;
    }

    public function engaged() : Amount
    {
        if (empty($this->plan)) {
            return Amount::zero();
        }
        return Amount::sum(...array_values($this->engaged));
    }

    public function isRoot() : bool
    {
        return empty($this->parentUuid);
    }

    protected function applyActionCreated(Event\ActionCreated $event)
    {
        $this->uuid = ActionUuid::fromString($event->aggregateId());
        $this->side = new BudgetSide($event->side());
        $this->type = new ActionType($event->type());
        $this->symbol = new ActionSymbol($event->symbol());
    }

    protected function applyActionPlanIncreased(Event\ActionPlanIncreased $event)
    {
        if(empty($this->plan[$event->atDate()->toString()])) {
            $this->plan[$event->atDate()->toString()] = Amount::zero();
        }

        $this->plan[$event->atDate()->toString()] = $this->plan[$event->atDate()->toString()]->add($event->amount());
    }

    protected function applyActionPlanDecreased(Event\ActionPlanDecreased $event)
    {
        if(empty($this->plan[$event->atDate()->toString()])) {
            $this->plan[$event->atDate()->toString()] = Amount::zero();
        }

        $this->plan[$event->atDate()->toString()] = $this->plan[$event->atDate()->toString()]->subtract($event->amount());
    }

    protected function applyActionEngaged(Event\ActionEngaged $event)
    {
        if(empty($this->engaged[$event->atDate()->toString()])) {
            $this->engaged[$event->atDate()->toString()] = Amount::zero();
        }

        $this->engaged[$event->atDate()->toString()] = $this->engaged[$event->atDate()->toString()]->add($event->amount());
    }

    protected function applyActionUnengaged(Event\ActionUnengaged $event)
    {
        if(empty($this->engaged[$event->atDate()->toString()])) {
            $this->engaged[$event->atDate()->toString()] = Amount::zero();
        }

        $this->engaged[$event->atDate()->toString()] = $this->engaged[$event->atDate()->toString()]->subtract($event->amount());
    }
}
<?php

namespace App\PlanningV3\Domain\Action;
use App\Common\UuidTrait;

final class ActionUuid
{
    use UuidTrait;
}

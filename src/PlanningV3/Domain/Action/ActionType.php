<?php

namespace App\PlanningV3\Domain\Action;

use App\Common\Enum;

/**
 * Class ActionType
 * @package App\PlanningV3\Domain\Action
 * @method static ActionType SECTION()
 * @method static ActionType SUBSECTION()
 * @method static ActionType PARAGRAPH()
 * @method static ActionType GROUP()
 * @method static ActionType TASK()
 * @method static ActionType SUBTASK()
 * @method static ActionType ACTION()
 * @method static ActionType SUBACTION()
 */
final class ActionType extends Enum
{
    private const SECTION = 'section';
    private const SUBSECTION = 'subsection';
    private const PARAGRAPH = 'paragraph';
    private const GROUP = 'group';
    private const TASK = 'task';
    private const SUBTASK = 'subtask';
    private const ACTION = 'action';
    private const SUBACTION = 'subaction';
}
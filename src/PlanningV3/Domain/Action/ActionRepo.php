<?php

namespace App\PlanningV3\Domain\Action;

interface ActionRepo
{
    public function getByUuid(ActionUuid $uuid): Action;
    public function save(Action $action);
}
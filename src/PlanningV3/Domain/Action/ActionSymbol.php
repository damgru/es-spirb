<?php

namespace App\PlanningV3\Domain\Action;

use App\Common\DomainException;

final class ActionSymbol
{
    private string $symbol;

    /**
     * ExpenseSymbol constructor.
     * @param string $symbol
     * @throws DomainException
     */
    public function __construct(string $symbol)
    {
        if (stristr($symbol, ' ') !== false) {
            throw new DomainException();
        }
        $this->symbol = $symbol;
    }

    /**
     * @return string
     */
    public function toString(): string
    {
        return $this->symbol;
    }
}
<?php

namespace App\PlanningV3\Domain\Action\Event;

use App\Common\Aggregate\AggregateChanged;
use App\Common\Amount;
use App\Common\Date;

final class ActionPlanDecreased extends AggregateChanged
{
    public static function eventName(): string
    {
        return 'p3_action_plan_decreased';
    }

    public static function with(string $actionUuid, Amount $amount, Date $atDate)
    {
        return new static($actionUuid, [
            'amount' => $amount->inPennies(),
            'at_date' => $atDate->toString()
        ]);
    }

    public function amount() : Amount
    {
        return Amount::fromPennies($this->payload['amount']);
    }

    public function atDate() : Date
    {
        return Date::fromString($this->payload['at_date']);
    }
}
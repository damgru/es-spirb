<?php

namespace App\PlanningV3\Domain\Action\Event;

use App\Common\Aggregate\AggregateChanged;

final class ActionCreated extends AggregateChanged
{
    public static function with(string $actionUuid, string $budgetSide, string $typeName, string $symbol)
    {
        return new static($actionUuid, [
            'side' => $budgetSide,
            'type' => $typeName,
            'symbol' => $symbol
        ]);
    }

    public static function eventName(): string
    {
        return 'p3_action_created';
    }

    public function side() {
        return $this->payload['side'];
    }

    public function type() {
        return $this->payload['type'];
    }

    public function symbol() {
        return $this->payload['symbol'];
    }
}
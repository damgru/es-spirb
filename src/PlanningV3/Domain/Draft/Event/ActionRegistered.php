<?php

namespace App\PlanningV3\Domain\Draft\Event;

use App\Common\Aggregate\AggregateChanged;

final class ActionRegistered extends AggregateChanged
{
    public static function eventName(): string
    {
        return 'p3_draft_action_registered';
    }

    public static function with(string $aggregateId, string $actionUuid)
    {
        return new static($aggregateId, [
            'action_uuid' => $actionUuid
        ]);
    }

    public function actionUuid() : string
    {
        return $this->payload['action_uuid'];
    }
}
<?php

namespace App\PlanningV3\Domain\Draft;

use App\Common\UuidTrait;

final class DraftUuid
{
    use UuidTrait;
}
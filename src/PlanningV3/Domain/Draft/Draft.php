<?php
namespace App\PlanningV3\Domain\Draft;

use App\Common\Aggregate\AggregateChanged;
use App\Common\Aggregate\AggregateRoot;
use App\Common\Aggregate\ApplyByApplyClassShortNameTrait;
use App\Common\Uuid;
use App\PlanningV3\Domain\Action\ActionUuid;

final class Draft extends AggregateRoot
{
    use ApplyByApplyClassShortNameTrait;
    private int $id;
    private DraftUuid $uuid;

    /** @var Uuid[] */
    private array $actions = [];

    public function aggregateId(): string
    {
        return $this->uuid->toString();
    }

    public function databaseId(): int
    {
        return $this->id;
    }

    public function register(ActionUuid $actionUuid): void
    {
        $this->record(Event\ActionRegistered::with($this->aggregateId(), $actionUuid->toString()));
    }

    protected function applyActionRegistered(Event\ActionRegistered $event)
    {
        $this->actions[$event->actionUuid()] = $event->actionUuid();
    }
}
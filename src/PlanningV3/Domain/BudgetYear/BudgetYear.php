<?php

namespace App\PlanningV3\Domain\BudgetYear;

use App\Common\Aggregate\AggregateRoot;
use App\Common\Aggregate\ApplyByApplyClassShortNameTrait;
use App\Common\Uuid;
use App\PlanningV3\Domain\Draft\DraftUuid;
use App\PlanningV3\Domain\ImmutablePlan\ImmutablePlanUuid;

final class BudgetYear extends AggregateRoot
{
    use ApplyByApplyClassShortNameTrait;

    private BudgetYearSettings $settings;
    private Year $year;
    private YearState $state;

    private DraftUuid $workingDraftUuid;

    private ImmutablePlanUuid $prognoseUuid;
    private ImmutablePlanUuid $propositionUuid;
    private ImmutablePlanUuid $projectUuid;
    private ImmutablePlanUuid $outgoingUuid;
    private ImmutablePlanUuid $currentUuid;

    /**
     * @var ImmutablePlanUuid[]
     */
    private array $plansByDate = [];

    public static function create(Year $year) : BudgetYear
    {
        $by = new BudgetYear();
        $by->record(Event\BudgetYearStarted::with($year));
        return $by;
    }

    public function attachDraft(DraftUuid $draftUuid)
    {
        $this->record(Event\DraftAttached::with($this->aggregateId(), $draftUuid));
    }

    public function aggregateId(): string
    {
        return (string)($this->year);
    }

    protected function applyBudgetYearStarted(Event\BudgetYearStarted $event)
    {
        $this->year = $event->year();
        $this->state = YearState::PROGNOSE();
        $this->settings = new BudgetYearSettings();
    }
}
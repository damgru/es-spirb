<?php

namespace App\PlanningV3\Domain\BudgetYear;

use App\Common\UuidTrait;

final class BudgetYearUuid
{
    use UuidTrait;
}
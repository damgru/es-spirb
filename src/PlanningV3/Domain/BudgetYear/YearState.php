<?php

namespace App\PlanningV3\Domain\BudgetYear;

use App\Common\Enum;

/**
 * Class YearState
 * @package App\PlanningV3\Domain\BudgetYear
 * @method static PROGNOSE()
 * @method static PROPOSITION()
 * @method static PROJECT()
 * @method static OUTGOING()
 * @method static CHANGES()
 * @method static ARCHIVE()
 */
final class YearState extends Enum
{
    const PROGNOSE      = 'PG';
    const PROPOSITION   = 'PR';
    const PROJECT       = 'P';
    const OUTGOING      = 'U';
    const CHANGES       = 'Z';
    const ARCHIVE       = 'A';
}
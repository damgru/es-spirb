<?php

namespace App\PlanningV3\Domain\BudgetYear;

final class Year
{
    private int $year;

    /**
     * Year constructor.
     * @param int $year
     */
    public function __construct(int $year)
    {
        $this->year = $year;
    }

    public function toString()
    {
        return strval($this->year);
    }

    public function toInt()
    {
        return $this->year;
    }

}
<?php

namespace App\PlanningV3\Domain\BudgetYear\Event;

use App\Common\Aggregate\AggregateChanged;
use App\PlanningV3\Domain\Draft\DraftUuid;

final class DraftAttached extends AggregateChanged
{
    public static function eventName(): string
    {
        return 'p3_budget_year_draft_attached';
    }

    public static function with(string $aggregateId, \App\PlanningV3\Domain\Draft\DraftUuid $draftUuid)
    {
        return new static($aggregateId, [
            'draft_uuid' => $draftUuid->toString()
        ]);
    }

    public function draftUuid(): DraftUuid
    {
        return DraftUuid::fromString($this->payload['draft_uuid']);
    }
}
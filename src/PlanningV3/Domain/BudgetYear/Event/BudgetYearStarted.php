<?php

namespace App\PlanningV3\Domain\BudgetYear\Event;

use App\Common\Aggregate\AggregateChanged;
use App\PlanningV3\Domain\BudgetYear\Year;

final class BudgetYearStarted extends AggregateChanged
{
    public static function eventName(): string
    {
        return 'p3_budget_year_started';
    }

    public function with(Year $year)
    {
        return new static($year->toString());
    }

    public function year(): Year
    {
        return new Year(intval($this->aggregateId));
    }
}
<?php

namespace App\PlanningV3\Infrastructure\EventSourcing;

use App\Common\Aggregate\AggregateRepository;
use App\PlanningV3\Domain\Action\Action;
use App\PlanningV3\Domain\Action\ActionRepo;
use App\PlanningV3\Domain\Action\ActionUuid;

final class ActionRepoEventSourced extends AggregateRepository implements ActionRepo
{
    public function getByUuid(ActionUuid $uuid): Action
    {
        return $this->loadAggregateRoot(Action::class, $uuid->toString());
    }

    public function save(Action $action)
    {
        $this->saveAggregateRoot($action);
    }
}
<?php

namespace App\PlanningV3\Infrastructure\Dbal;

use App\PlanningV3\Domain\ImmutablePlan\ImmutablePlan;
use App\PlanningV3\Domain\ImmutablePlan\ImmutablePlanRepo;
use App\PlanningV3\Domain\ImmutablePlan\ImmutablePlanUuid;
use Doctrine\DBAL\Driver\Connection;

final class ImmutablePlanRepoDbal implements ImmutablePlanRepo
{
    private Connection $connection;

    /**
     * ImmutablePlanRepoDbal constructor.
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function save(ImmutablePlan $plan)
    {
        if($plan->id() > 0) {
            $this->update($plan);
        }
        else {
            $this->addNew($plan);
        }
    }

    public function getByUuid(ImmutablePlanUuid $uuid): ImmutablePlan
    {

    }

    private function addNew(ImmutablePlan $plan)
    {

    }

    private function update(ImmutablePlan $plan)
    {

    }
}
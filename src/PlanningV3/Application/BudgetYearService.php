<?php

namespace App\PlanningV3\Application;

use App\Planning\Domain\BudgetYear\BudgetYearRepository;

final class BudgetYearService
{
    private BudgetYearRepository $yearRepo;

    /**
     * BudgetYearService constructor.
     * @param BudgetYearRepository $yearRepo
     */
    public function __construct(BudgetYearRepository $yearRepo)
    {
        $this->yearRepo = $yearRepo;
    }
}
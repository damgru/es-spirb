<?php

namespace App\Command;

use App\Common\ReadModel\ReadModelManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ReadModelsDropCommand extends Command
{
    protected static $defaultName = 'app:read-models:drop';
    /** @var ReadModelManager */
    private $manager;

    /**
     * ReadModelsDropCommand constructor.
     * @param ReadModelManager $manager
     */
    public function __construct(ReadModelManager $manager)
    {
        $this->manager = $manager;
        parent::__construct();
    }


    protected function configure()
    {
        $this->setDescription('Drops all read-models');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $this->manager->dropAll();
        $io->success('you dropped all of this');
    }
}

<?php

namespace App\Command;

use App\Common\ReadModel\ReadModelManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ReadModelsInitCommand extends Command
{
    protected static $defaultName = 'app:read-models:init';
    /** @var ReadModelManager */
    private $manager;

    /**
     * ReadModelsInitCommand constructor.
     * @param ReadModelManager $manager
     */
    public function __construct(ReadModelManager $manager)
    {
        parent::__construct();
        $this->manager = $manager;
    }


    protected function configure()
    {
        $this->setDescription('Init read-models');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $this->manager->init();
        $io->success('Okay');
    }
}

<?php

namespace App\Command;

use App\Common\ReadModel\ReadModelManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ReadModelsTriggerCommand extends Command
{
    protected static $defaultName = 'app:read-models:trigger';

    /** @var ReadModelManager */
    private $readModelManager;

    /**
     * ReadModelsTriggerCommand constructor.
     * @param ReadModelManager $readModelManager
     */
    public function __construct(ReadModelManager $readModelManager)
    {
        $this->readModelManager = $readModelManager;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('trigerrs all read-models')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $this->readModelManager->trigger();
        $io->success('okey');
    }
}

<?php

namespace App\Common;

use Ramsey\Uuid\UuidInterface as RamseyUuidInterface;

trait UuidTrait
{
    /** @var RamseyUuidInterface */
    private $ramseyUuid;

    /**
     * Uuid constructor.
     * @param RamseyUuidInterface $ramseyUuid
     */
    private function __construct(RamseyUuidInterface $ramseyUuid)
    {
        $this->ramseyUuid = $ramseyUuid;
    }

    /**
     * @throws \Exception
     * @return static
     */
    public static function generate()
    {
        return new static(\Ramsey\Uuid\Uuid::uuid4());
    }

    /**
     * @param $string
     * @return static
     */
    public static function fromString($string)
    {
        return new static(\Ramsey\Uuid\Uuid::fromString($string));
    }

    public function toString() : string
    {
        return $this->ramseyUuid->toString();
    }

    public function __toString()
    {
        return $this->toString();
    }
}
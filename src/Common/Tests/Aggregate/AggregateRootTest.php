<?php

namespace App\Common\Tests\Aggregate;

use App\Common\Aggregate\AggregateChanged;
use App\Common\Aggregate\AggregateRoot;
use App\Common\DomainException;
use PHPUnit\Framework\TestCase;

class AggregateRootTest extends TestCase
{
    /** @var AggregateRoot */
    private $aggregateRoot;

    protected function setUp() : void
    {
        parent::setUp();

        $this->aggregateRoot = new class extends AggregateRoot {

            public $applyCount = 0;
            public $eventNames = [];

            /**
             * @param AggregateChanged $event
             * @throws DomainException
             */
            public function apply(AggregateChanged $event): void
            {
                $this->applyCount++;
                $this->eventNames[] = $event->eventName();
            }

            public function aggregateId(): string
            {
                return 'af5b2216-1a4e-4794-8d89-c8740e6bfebd';
            }

            public function doSomeEvent() {
                $event = new class extends AggregateChanged
                {
                    public function __construct() {
                        parent::__construct(
                            'ee081a5d-48dd-44dc-9db4-9e52b651a87e'
                        );
                    }

                    public static function eventName(): string
                    {
                        return 'some-event';
                    }

                    public function eventDescription(): string
                    {
                        return 'some-event';
                    }
                };

                $this->record($event);
            }
        };
    }

    public function testRecord()
    {
        $this->aggregateRoot->doSomeEvent();

        $events = $this->aggregateRoot->getUncommittedEvents();

        $this->assertCount(1, $events);
        $this->assertEquals(1, $this->aggregateRoot->applyCount);
    }

    public function testMarkAsCommited()
    {
        $this->aggregateRoot->doSomeEvent();

        $this->aggregateRoot->markEventsAsCommitted();

        $events = $this->aggregateRoot->getUncommittedEvents();
        $this->assertCount(0, $events);
    }
}

<?php

namespace App\Common;

final class Date
{
    private \DateTimeImmutable $date;

    /**
     * Date constructor.
     */
    private function __construct(\DateTimeImmutable $dateTimeImmutable)
    {
        $this->date = $dateTimeImmutable;
    }

    public static function fromDateTimeImmutable(\DateTimeImmutable $dateTimeImmutable) : self
    {
        return new self($dateTimeImmutable);
    }

    public static function fromString($string) : self
    {
        $time = new \DateTimeImmutable($string);
        return self::fromDateTimeImmutable($time);
    }

    public function diff(Date $date) : \DateInterval
    {
        return $this->date->diff($date->date);
    }

    public function isBefore(Date $date) : bool
    {
        return $this->date < $date->date;
    }

    public function isBeforeOrEqual(Date $date) : bool
    {
        return $this->date <= $date->date;
    }

    public function isAfter(Date $date) : bool
    {
        return $this->date > $date->date;
    }

    public function isAfterOrEqual(Date $date) : bool
    {
        return $this->date >= $date->date;
    }

    public function equals(Date $date) : bool
    {
        return $this->date === $date->date;
    }

    public function toString()
    {
        return $this->date->format('Y-m-d');
    }

    /** @return int */
    public function year(): int
    {
        // Y - A full numeric representation of a year, 4 digits
        return intval($this->date->format('Y'));
    }

    /** @return int */
    public function month(): int
    {
        // n - Numeric representation of a month, without leading zeros
        return intval($this->date->format('n'));
    }

    /** @return int */
    public function day(): int
    {
        // j - Day of the month without leading zeros
        return intval($this->date->format('j'));
    }


}
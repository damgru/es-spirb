<?php

namespace App\Common;

use App\Common\Command;

interface CommandBus
{
    public function handle(Command $command): void;
}
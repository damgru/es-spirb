<?php


namespace App\Common\Infrastructure\DynamicEventFactory;

use App\Common\Aggregate\AggregateChanged;
use App\Common\Aggregate\EventFactory;
use Symfony\Component\DependencyInjection\Reference;

class DynamicEventFactory implements EventFactory
{
    private $type = [];

    public function registerEvent($reference)
    {
        $eventName = $reference::eventName();
        if(isset($this->type[$eventName])) {
            throw new \RuntimeException("Cannot register event $eventName from class $reference. Class {$this->type[$eventName]} has same eventName.");
        }
        $this->type[$eventName] = $reference;
    }

    public function createFromName(
        string $eventName,
        string $aggregateId,
        array $payload,
        \DateTimeImmutable $created = null,
        int $version = 0,
        string $category,
        int $id = null)
    : AggregateChanged
    {
        return new $this->type[$eventName]($aggregateId, $payload, $created, $version, $category, $id);
    }
}
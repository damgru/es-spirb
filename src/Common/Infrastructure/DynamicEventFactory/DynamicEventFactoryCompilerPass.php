<?php


namespace App\Common\Infrastructure\DynamicEventFactory;


use App\Common\Infrastructure\DynamicEventFactory\DynamicEventFactory;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class DynamicEventFactoryCompilerPass implements CompilerPassInterface
{
    /**
     * EventAutoLoaderCompilerPass constructor.
     */
    public function __construct()
    {
    }

    /**
     * You can modify the container here before it is dumped to PHP code.
     */
    public function process(ContainerBuilder $container)
    {
        if(!$container->has(DynamicEventFactory::class)) {
            return;
        }

        $definition = $container->findDefinition(DynamicEventFactory::class);
        $taggedServices = $container->findTaggedServiceIds('app.event');
        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall(
                'registerEvent',
                array($id)
            );
        }
    }
}
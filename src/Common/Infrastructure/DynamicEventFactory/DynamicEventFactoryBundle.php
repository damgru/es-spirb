<?php


namespace App\Common\Infrastructure\DynamicEventFactory;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class DynamicEventFactoryBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new DynamicEventFactoryCompilerPass());
    }
}
<?php

namespace App\Common\Infrastructure;

use App\Common\Command;
use App\Common\CommandBus;
use Doctrine\DBAL\Driver\Connection;
use Exception;

final class TransactionalCommandBus implements CommandBus
{
    /** @var Connection */
    private $connection;

    /** @var CommandBus */
    private $commmandBus;

    /**
     * TransactionalCommandBus constructor.
     * @param Connection $connection
     * @param CommandBus $commmandBus
     */
    public function __construct(Connection $connection, CommandBus $commmandBus)
    {
        $this->connection = $connection;
        $this->commmandBus = $commmandBus;
    }

    public function handle(Command $command): void
    {
        try {
            $this->connection->beginTransaction();
            $this->commmandBus->handle($command);
            $this->connection->commit();
        }
        catch (Exception $exception) {
            $this->connection->rollBack();
            throw $exception;
        }
    }
}
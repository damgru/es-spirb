<?php
namespace App\Common\Infrastructure;

use App\Common\Aggregate\AggregateChanged;
use App\Common\EventBus;
use Symfony\Component\Messenger\MessageBusInterface;

class SymfonyEventBus implements EventBus
{
    /** @var MessageBusInterface */
    private $eventBus;

    /**
     * SymfonyCommandBus constructor.
     * @param MessageBusInterface $eventBus
     */
    public function __construct(MessageBusInterface $eventBus)
    {
        $this->eventBus = $eventBus;
    }

    public function dispatch(AggregateChanged $event): void
    {
        $this->eventBus->dispatch($event);
    }
}
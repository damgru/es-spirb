<?php
namespace App\Common\Infrastructure;

use App\Common\Command;
use App\Common\CommandBus;
use Symfony\Component\Messenger\MessageBusInterface;

class SymfonyCommandBus implements CommandBus
{
    /** @var MessageBusInterface */
    private $messageBus;

    /**
     * SymfonyCommandBus constructor.
     * @param MessageBusInterface $messageBus
     */
    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    public function handle(Command $command): void
    {
        $this->messageBus->dispatch($command);
    }
}
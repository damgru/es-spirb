<?php

namespace App\Common\Infrastructure\EventStore;

use App\Common\Aggregate\AggregateChanged;
use App\Common\Aggregate\EventStore;
use App\Common\EventBus;
use DateTimeImmutable;

class EventStoreWithEventBus implements EventStore
{
    /** @var EventStore */
    private $eventStore;

    /** @var EventBus */
    private $eventBus;


    /**
     * EventStoreWithEventBus constructor.
     * @param EventStore $eventStore
     * @param EventBus $eventBus
     */
    public function __construct(EventStore $eventStore, EventBus $eventBus)
    {
        $this->eventStore = $eventStore;
        $this->eventBus = $eventBus;
    }

    /**
     * @param AggregateChanged[] $events
     */
    public function add(AggregateChanged ...$events): void
    {
        $this->eventStore->add(...$events);
        foreach ($events as $event) {
            $this->eventBus->dispatch($event);
        }
    }

    /**
     * @param string $aggregateId
     * @return iterable<AggregateChanged>
     */
    public function getEvents(string $aggregateId): iterable
    {
        return $this->eventStore->getEvents($aggregateId);
    }

    /**
     * @return iterable<AggregateChanged>
     */
    public function getAllEvents(): iterable
    {
        return $this->eventStore->getAllEvents();
    }

    /**
     * @param string $aggregateId
     * @param int $version
     * @param int $limit
     * @return iterable<AggregateChanged>
     */
    public function getEventsFromVersion(string $aggregateId, int $version, $limit = 0): iterable
    {
        return $this->eventStore->getEventsFromVersion($aggregateId, $version, $limit);
    }

    /**
     * @param string $aggregateId
     * @param int $version
     * @param int $limit
     * @return iterable<AggregateChanged>
     */
    public function getEventsToVersion(string $aggregateId, int $version, $limit = 0): iterable
    {
        return $this->eventStore->getEventsToVersion($aggregateId, $version, $limit);
    }

    /**
     * @param DateTimeImmutable $timeImmutable
     * @param int $limit
     * @return iterable<AggregateChanged>
     */
    public function getEventsNewerThan(DateTimeImmutable $timeImmutable, $limit = 0): iterable
    {
        return $this->eventStore->getEventsNewerThan($timeImmutable, $limit);
    }

    /**
     * @param DateTimeImmutable $timeImmutable
     * @param int $limit
     * @return iterable<AggregateChanged>
     */
    public function getEventsToDate(DateTimeImmutable $timeImmutable, $limit = 0): iterable
    {
        return $this->getEventsToDate($timeImmutable, $limit);
    }
}
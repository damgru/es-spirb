<?php
namespace App\Common\Infrastructure\EventStore;

use App\Common\Aggregate\AggregateChanged;
use App\Common\Aggregate\EventFactory;
use App\Common\Aggregate\EventStore;
use App\Common\Uuid;
use DateTimeImmutable;
use Doctrine\DBAL\Driver\Connection;
use Exception;

class DbalEventStore implements EventStore
{
    /** @var EventFactory */
    private $eventFactory;
    /** @var Connection */
    private $connection;

    /**
     * DbalEventStore constructor.
     * @param Connection $connection
     * @param EventFactory $eventFactory
     */
    public function __construct(Connection $connection, EventFactory $eventFactory)
    {
        $this->eventFactory = $eventFactory;
        $this->connection = $connection;
    }

    /**
     * @param AggregateChanged[] $events
     */
    public function add(AggregateChanged ...$events): void
    {
        $sql = <<<SQL
            INSERT INTO events (aggregate_id, event_name, payload, created, version, category) 
            VALUES             
SQL;
        $sql .= implode(',', array_fill(0, count($events), '(?,?,?,?,?,?)'));
        $stmt = $this->connection->prepare($sql);
        $params = [];
        foreach ($events as $event)
        {
            array_push($params,
                $event->aggregateId(),
                $event->eventName(),
                json_encode($event->payload()),
                $event->created()->format('Y-m-d H:i:s.u'),
                $event->version(),
                $event->category()
            );
        }

        $stmt->execute($params);
    }

    /**
     * @param string $aggregateId
     * @return iterable<AggregateChanged>
     * @throws Exception
     */
    public function getEvents(string $aggregateId): iterable
    {
        $sql = <<<SQL
SELECT * FROM events WHERE aggregate_id = ? ORDER BY created, version;
SQL;
        $stmt = $this->connection->prepare($sql);

        $stmt->execute([
            $aggregateId
        ]);
        $rows = $stmt->fetchAll();
        foreach ($rows as $row) {
            yield $this->eventFromRow($row);
        }
    }

    /**
     * @return iterable<AggregateChanged>
     * @throws Exception
     */
    public function getAllEvents(): iterable
    {
        $sql = <<<SQL
SELECT * 
FROM events 
ORDER BY created, version ASC;
SQL;
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $rows = $stmt->fetchAll();
        foreach ($rows as $row) {
            yield $this->eventFromRow($row);
        }
    }

    /**
     * @param Uuid $aggregateId
     * @param int $version
     * @param int $limit
     * @return iterable<AggregateChanged>
     * @throws Exception
     */
    public function getEventsFromVersion(string $aggregateId, int $version, $limit = 0): iterable
    {
        $sql = <<<SQL
SELECT * 
FROM events 
WHERE aggregate_id = :aggregate_id 
  AND version > :version 
ORDER BY created, version
SQL;
        if($limit > 0) {
            $sql .= " LIMIT :limit";
        }

        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue('aggregate_id', $aggregateId);
        $stmt->bindValue('version', $version);

        if($limit > 0) {
            $stmt->bindValue('limit', $limit);
        }

        $stmt->execute();
        $rows = $stmt->fetchAll();
        foreach ($rows as $row) {
            yield $this->eventFromRow($row);
        }
    }

    /**
     * @param $row
     * @return AggregateChanged
     * @throws Exception
     */
    private function eventFromRow($row): AggregateChanged
    {
        return $this->eventFactory->createFromName(
            $row['event_name'],
            $row['aggregate_id'],
            json_decode($row['payload'], true),
            new DateTimeImmutable($row['created']),
            $row['version'],
            $row['category'],
            $row['id']
        );
    }

    /**
     * @param Uuid $aggregateId
     * @param int $version
     * @param int $limit
     * @return iterable<AggregateChanged>
     * @throws Exception
     */
    public function getEventsToVersion(string $aggregateId, int $version, $limit = 0): iterable
    {
        $sql = <<<SQL
SELECT * 
FROM events 
WHERE aggregate_id = :aggregate_id 
  AND version <= :version 
ORDER BY created, version
SQL;
        if($limit > 0) {
            $sql .= " LIMIT :limit";
        }

        $stmt = $this->connection->prepare($sql);
        $stmt->bindParam('aggregate_id', $aggregateId->toString());
        $stmt->bindParam('version', $version);

        if($limit > 0) {
            $stmt->bindParam('limit', $limit);
        }

        $stmt->execute();
        $rows = $stmt->fetchAll();
        foreach ($rows as $row) {
            yield $this->eventFromRow($row);
        }
    }

    /**
     * @param int $id
     * @param int $limit
     * @return iterable<AggregateChanged>
     * @throws Exception
     */
    public function getEventsNewerThan(DateTimeImmutable $timeImmutable, $limit = 0): iterable
    {
        $sql = <<<SQL
SELECT * 
FROM events 
WHERE created > :created 
ORDER BY created, version
SQL;
        if($limit > 0) {
            $sql .= " LIMIT :limit";
        }

        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue('created', $timeImmutable->format('Y-m-d H:i:s.u'));

        if($limit > 0) {
            $stmt->bindValue('limit', $limit);
        }

        $stmt->execute();
        $rows = $stmt->fetchAll();
        foreach ($rows as $row) {
            yield $this->eventFromRow($row);
        }
    }

    /**
     * @param int $id
     * @param int $limit
     * @return iterable<AggregateChanged>
     * @throws Exception
     */
    public function getEventsToDate(DateTimeImmutable $timeImmutable, $limit = 0): iterable
    {
        $sql = <<<SQL
SELECT * 
FROM events 
WHERE created <= :created 
ORDER BY created, version
SQL;
        if($limit > 0) {
            $sql .= " LIMIT :limit";
        }

        $stmt = $this->connection->prepare($sql);
        $stmt->bindParam('created', $timeImmutable->format('Y-m-d H:i:s.u'));

        if($limit > 0) {
            $stmt->bindParam('limit', $limit);
        }

        $stmt->execute();
        $rows = $stmt->fetchAll();
        foreach ($rows as $row) {
            yield $this->eventFromRow($row);
        }
    }
}
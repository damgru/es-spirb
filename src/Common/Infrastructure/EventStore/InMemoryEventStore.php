<?php
namespace App\Common\Infrastructure\EventStore;

use App\Common\Aggregate\AggregateChanged;
use App\Common\Aggregate\EventStore;
use DateTimeImmutable;

class InMemoryEventStore implements EventStore
{
    /** @var AggregateChanged[] */
    private $events = [];

    /**
     * InMemoryEventStore constructor.
     * @param AggregateChanged[] $events
     */
    public function __construct(AggregateChanged ...$events)
    {
        $this->events = $events;
    }

    /**
     * @param AggregateChanged ...$events
     */
    public function add(AggregateChanged ...$events): void
    {
        $this->events = array_merge($this->events, $events);
    }


    /**
     * @param string $aggregateId
     * @return iterable<AggregateChanged>
     */
    public function getEvents(string $aggregateId): iterable
    {
        foreach ($this->events as $event) {
            if ($event->aggregateId() === $aggregateId) {
                yield $event;
            }
        }
    }

    /**
     * @return iterable<AggregateChanged>
     */
    public function getAllEvents(): iterable
    {
        return $this->events;
    }

    /**
     * @param string $aggregateId
     * @param int $version
     * @param int $limit
     * @return iterable<AggregateChanged>
     */
    public function getEventsFromVersion(string $aggregateId, int $version, $limit = 0): iterable
    {
        $limit = empty($limit) ? -1 : $limit;
        foreach ($this->events as $event) {
            if ($event->aggregateId() === $aggregateId && $event->version() > $version) {
                yield $event;
                $limit--;
            }
            if ($limit == 0) break;
        }
    }

    /**
     * @param string $aggregateId
     * @param int $version
     * @param int $limit
     * @return iterable<AggregateChanged>
     */
    public function getEventsToVersion(string $aggregateId, int $version, $limit = 0): iterable
    {
        $limit = empty($limit) ? -1 : $limit;
        foreach ($this->events as $event) {
            if ($event->aggregateId() === $aggregateId && $event->version() <= $version) {
                yield $event;
                $limit--;
            }
            if ($limit == 0) break;
        }
    }

    /**
     * @param DateTimeImmutable $timeImmutable
     * @param int $limit
     * @return iterable<AggregateChanged>
     */
    public function getEventsNewerThan(DateTimeImmutable $timeImmutable, $limit = 0): iterable
    {
        $limit = empty($limit) ? -1 : $limit;
        foreach ($this->events as $event) {
            if ($event->created() > $timeImmutable) {
                yield $event;
                $limit--;
            }
            if ($limit == 0) break;
        }
    }

    /**
     * @param DateTimeImmutable $timeImmutable
     * @param int $limit
     * @return iterable<AggregateChanged>
     */
    public function getEventsToDate(DateTimeImmutable $timeImmutable, $limit = 0): iterable
    {
        $limit = empty($limit) ? -1 : $limit;
        foreach ($this->events as $event) {
            if ($event->created() <= $timeImmutable) {
                yield $event;
                $limit--;
            }
            if ($limit == 0) break;
        }
    }
}
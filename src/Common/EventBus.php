<?php

namespace App\Common;

use App\Common\Aggregate\AggregateChanged;

interface EventBus
{
    public function dispatch(AggregateChanged $event): void ;
}
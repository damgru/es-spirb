<?php
namespace App\Common\ReadModel;

interface ReadModelManager
{
    public function registerReadModel(ReadModel $readModel);
    public function trigger($specificClass = null);
    public function drop(ReadModel $readModel);
    public function init();
    public function getReadModel($class): ReadModel;
    public function dropAll();
}
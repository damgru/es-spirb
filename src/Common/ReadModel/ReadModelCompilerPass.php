<?php


namespace App\Common\ReadModel;

use App\Common\Infrastructure\DynamicEventFactory\DynamicEventFactory;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class ReadModelCompilerPass implements CompilerPassInterface
{
    /**
     * You can modify the container here before it is dumped to PHP code.
     */
    public function process(ContainerBuilder $container)
    {
        if(!$container->has(DbalReadModelManager::class)) {
            return;
        }

        $definition = $container->findDefinition(ReadModelManager::class);
        $taggedServices = $container->findTaggedServiceIds('app.readmodel');
        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall(
                'withReadModel',
                [new Reference($id)]
            );
        }
    }
}
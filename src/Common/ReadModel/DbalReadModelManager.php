<?php
namespace App\Common\ReadModel;
use App\Common\Aggregate\EventStore;
use Doctrine\DBAL\Driver\Connection;

class DbalReadModelManager implements ReadModelManager
{
    const TIME_FORMAT = 'Y-m-d H:i:s.u';

    /** @var Connection */
    protected $connection;

    /** @var EventStore */
    protected $eventStore;

    /** @var ReadModel[] */
    protected $readModels = [];

    /**
     * DbalReadModelManager constructor.
     * @param Connection $connection
     * @param EventStore $eventStore
     */
    public function __construct(Connection $connection, EventStore $eventStore)
    {
        $this->connection = $connection;
        $this->eventStore = $eventStore;
    }

    public function withReadModel(ReadModel $readModel)
    {
        $this->readModels[] = $readModel;
    }

    protected function readModels()
    {
        return $this->readModels;
    }

    public function init()
    {
        $sql = <<<SQL
            CREATE TABLE IF NOT EXISTS read_models (
                class text NOT NULL,
                last_event_time timestamp,
                CONSTRAINT read_models_pk UNIQUE (class) 
            )
SQL;
        $this->connection->exec($sql);
    }

    public function trigger($specificClass = null)
    {
        $updateStmt = $this->connection->prepare("UPDATE read_models SET last_event_time = :time WHERE class = :class");
        if(empty($specificClass)) {
            $selectStmt = $this->connection->prepare("SELECT * FROM read_models");
        } else {
            $selectStmt = $this->connection->prepare("SELECT * FROM read_models WHERE class = :class");
            $selectStmt->bindValue('class', $specificClass);
        }
        $selectStmt->execute();
        $rows = $selectStmt->fetchAll();
        $rows = array_column($rows, 'last_event_time', 'class');
        foreach ($this->readModels as $readModel)
        {
            $class = get_class($readModel);
            if(!isset($rows[$class])) {
                $this->registerReadModel($readModel);
                $lastEventTime = $readModel->lastEventTime();
            } else {
                $lastEventTime = new \DateTimeImmutable($rows[$class]);
            }
            $beforeEventTime = $lastEventTime;
            $readModel->withLastEventTime($lastEventTime);
            $readModel->update();

            if($beforeEventTime !== $readModel->lastEventTime()) {
                $updateStmt->bindValue('time', $readModel->lastEventTime()->format(self::TIME_FORMAT));
                $updateStmt->bindValue('class', $class);
                $updateStmt->execute();
            }
        }
    }

    public function drop(ReadModel $readModel)
    {
        $readModel->drop();
        $class = get_class($readModel);
        $this->connection->exec("DELETE FROM read_models WHERE class = '$class'");
    }

    public function registerReadModel(ReadModel $readModel)
    {
        $class = get_class($readModel);
        $this->connection->exec("INSERT INTO read_models (class, last_event_time) VALUES ('$class', '1980-01-01')");
        $readModel->init();
    }

    public function dropAll()
    {
        $readModels = $this->readModels();
        if(!empty($readModels)) {
            foreach ($readModels as $model) {
                $this->drop($model);
            }
        }
        $this->connection->exec("DROP TABLE IF EXISTS read_models");
    }

    public function getReadModel($class): ReadModel
    {
        foreach ($this->readModels as $readModel) {
            if (get_class($readModel) == $class) {
                return $readModel;
            }
        }
    }
}
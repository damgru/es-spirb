<?php
namespace App\Common\ReadModel;

interface ReadModel
{
    public function init();
    public function drop();
    public function update();
    public function withLastEventTime(\DateTimeImmutable $eventTime);
    public function lastEventTime() : \DateTimeImmutable;
}
<?php


namespace App\Common\ReadModel;

use App\Common\Aggregate\AggregateChanged;
use App\Common\Aggregate\ApplyByApplyClassShortNameTrait;
use App\Common\Aggregate\EventStore;
use Doctrine\DBAL\Driver\Connection;

abstract class DbalReadModel implements ReadModel
{
    use ApplyByApplyClassShortNameTrait;

    /** @var Connection */
    protected $connection;
    /** @var EventStore */
    protected $eventStore;
    /** @var \DateTimeImmutable */
    protected $lastEventTime;

    /**
     * DbalReadModel constructor.
     * @param Connection $connection
     */
    public function __construct(Connection $connection, EventStore $eventStore, ?\DateTimeImmutable $lastEventTime = null)
    {
        $this->connection = $connection;
        $this->eventStore = $eventStore;
        $this->lastEventTime = empty($lastEventTime)
            ? \DateTimeImmutable::createFromFormat('Y-m-d', '1980-01-01')
            : $lastEventTime;
    }

    public function update()
    {
        foreach ($this->eventStore->getEventsNewerThan($this->lastEventTime) as $event)
        {
            /** @var $event AggregateChanged */
            $this->apply($event);
            $this->lastEventTime = $event->created();
        }
    }

    public function lastEventTime() : \DateTimeImmutable
    {
        return $this->lastEventTime;
    }

    public function withLastEventTime(\DateTimeImmutable $eventTime)
    {
        $this->lastEventTime = $eventTime;
    }

    protected function executeSql($sql, $params = [])
    {
        $this->connection->prepare($sql)->execute($params);
    }

    protected function fetchAll($sql, $params)
    {
        $stmt = $this->connection->prepare($sql);
        $stmt->execute($params);
        return $stmt->fetchAll();
    }
}
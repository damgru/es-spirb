<?php
namespace App\Common;

use Money\Money;

class Amount
{
    private Money $money;

    private function __construct(Money $money)
    {
        $this->money = $money;
    }

    public static function fromString(string $amount) : Amount
    {
        return new Amount(Money::PLN($amount));
    }

    public static function fromPennies(int $amount) : Amount
    {
        return new Amount(Money::PLN($amount));
    }

    public static function fromDecimal(float $amount) : Amount
    {
        return new Amount(Money::PLN($amount*100));
    }

    public static function sum(Amount ...$amounts) : Amount
    {
        $sum = self::zero();
        $sum = $sum->add(...$amounts);
        return $sum;
    }

    public static function zero() : Amount
    {
        return self::fromPennies(0);
    }

    public function inPennies() : int
    {
        return $this->money->getAmount();
    }

    public function inZloty() : float
    {
        return $this->inPennies()/100.0;
    }

    public function add(Amount ...$addends)
    {
        $sum = Money::PLN($this->inPennies());
        foreach ($addends as $addend) {
            $sum = $sum->add($addend->money);
        }
        return new self($sum);
    }

    public function subtract(Amount ...$subtrahends)
    {
        $sum = Money::PLN($this->inPennies());
        foreach ($subtrahends as $subtrahend) {
            $sum = $sum->subtract($subtrahend->money);
        }
        return new self($sum);
    }

    public function multiply(Amount $multipier)
    {
        $this->money = $this->money->multiply($multipier->money);
    }

    public function divide(Amount $divisor)
    {
        $this->money = $this->money->divide($divisor->money);
    }

    public function greaterThan(Amount $amount)
    {
        return $this->money->greaterThan($amount->money);
    }

    public function greaterThanOrEqual(Amount $amount)
    {
        return $this->money->greaterThanOrEqual($amount->money);
    }

    public function lessThan(Amount $amount)
    {
        return $this->money->lessThan($amount->money);
    }

    public function lessThanOrEqual(Amount $amount)
    {
        return $this->money->lessThanOrEqual($amount->money);
    }

    public function equals(Amount $amount)
    {
        return $this->money->equals($amount->money);
    }

    public function isNegative()
    {
        return $this->money->isNegative();
    }

    public function isPositive()
    {
        return $this->money->isPositive();
    }

    public function isZero()
    {
        return $this->money->isZero();
    }
}
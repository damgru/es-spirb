<?php
/**
 * Created by PhpStorm.
 * User: dgrudzien
 * Date: 01.03.19
 * Time: 10:58
 */

namespace App\Common\Aggregate;

use DateTimeImmutable;

interface EventStore
{
    /**
     * @param AggregateChanged[] $events
     */
    public function add(AggregateChanged ...$events) : void;

    /**
     * @param string $aggregateId
     * @return iterable<AggregateChanged>
     */
    public function getEvents(string $aggregateId) : iterable;

    /**
     * @return iterable<AggregateChanged>
     */
    public function getAllEvents() : iterable;

    /**
     * @param string $aggregateId
     * @param int $version
     * @param int $limit
     * @return iterable<AggregateChanged>
     */
    public function getEventsFromVersion(string $aggregateId, int $version, $limit = 0) : iterable;

    /**
     * @param string $aggregateId
     * @param int $version
     * @param int $limit
     * @return iterable<AggregateChanged>
     */
    public function getEventsToVersion(string $aggregateId, int $version, $limit = 0) : iterable;

    /**
     * @param DateTimeImmutable $timeImmutable
     * @param int $limit
     * @return iterable<AggregateChanged>
     */
    public function getEventsNewerThan(DateTimeImmutable $timeImmutable, $limit = 0) : iterable;

    /**
     * @param DateTimeImmutable $timeImmutable
     * @param int $limit
     * @return iterable<AggregateChanged>
     */
    public function getEventsToDate(DateTimeImmutable $timeImmutable, $limit = 0) : iterable;
}
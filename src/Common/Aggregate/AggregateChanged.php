<?php

namespace App\Common\Aggregate;

abstract class AggregateChanged
{
    /** @var int */
    protected $id;

    /** @var string */
    protected $aggregateId;

    /** @var array */
    protected $payload = [];

    /** @var \DateTimeImmutable */
    protected $created;

    /** @var int */
    protected $version;

    /** @var string */
    protected $category;

    /**
     * AggregateChanged constructor.
     * @param string $aggregateId
     * @param array $payload
     * @param \DateTimeImmutable $created
     * @param int $version
     * @param string $category
     */
    public function __construct(string $aggregateId, array $payload = [], \DateTimeImmutable $created = null, int $version = 0, string $category = '', int $id = null)
    {
        $this->aggregateId = $aggregateId;
        $this->payload = $payload;
        if(empty($created)) {
            $created = \DateTimeImmutable::createFromFormat('0.u00 U', microtime());
        }
        $this->created = $created;
        $this->version = $version;
        $this->category = $category;
        $this->id = $id;
    }

    abstract public static function eventName(): string;

    public function eventDescription(): string
    {
        return $this::eventName().' '.json_encode($this->payload());
    }

    public function aggregateId(): string
    {
        return $this->aggregateId;
    }

    public function payload() : array
    {
        return $this->payload;
    }

    public function version() : int
    {
        return $this->version;
    }

    public function withVersion(int $version) : self
    {
        $this->version = $version;
        return $this;
    }

    public function created() : \DateTimeImmutable
    {
        return $this->created;
    }

    public function category() : string
    {
        return $this->category;
    }

    public function id() : int
    {
        return $this->id;
    }
}
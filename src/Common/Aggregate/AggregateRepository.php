<?php
/**
 * Created by PhpStorm.
 * User: dgrudzien
 * Date: 01.03.19
 * Time: 14:51
 */

namespace App\Common\Aggregate;

use ReflectionClass;
use ReflectionException;

abstract class AggregateRepository
{
    /** @var EventStore */
    private $eventStore;
    /** @var AggregateRoot[] */
    protected $roots = [];

    /**
     * AggregateRepository constructor.
     * @param EventStore $eventStore
     */
    public function __construct(EventStore $eventStore)
    {
        $this->eventStore = $eventStore;
    }

    public function saveAggregateRoot(AggregateRoot $aggregateRoot)
    {
        $events = $aggregateRoot->getUncommittedEvents();
        $this->eventStore->add(...$events);
        $aggregateRoot->markEventsAsCommitted();

        //cache
        $this->roots[$aggregateRoot->aggregateId()] = $aggregateRoot;
    }

    /**
     * @param $class
     * @param string $aggregateId
     * @return AggregateRoot
     * @throws AggregateException
     */
    public function loadAggregateRoot($class, string $aggregateId): AggregateRoot
    {
        if(!$this->isAggregateRootClass($class)){
            throw new AggregateException('You cannot load non AggregateRoot object');
        }

        //try use cache
        if(isset($this->roots[$aggregateId])) {
            $aggregateRoot = $this->roots[$aggregateId];
            // its possible to check, if this object is up-to-date, but we can assume, that in one request there are no changes
            // $events = $this->eventStore->getEventsFromVersion($aggregateId, $aggregateRoot->version());
            // if(!empty($events)) {
            //     foreach ($events as $event) {
            //         $aggregateRoot->apply($event);
            //     }
            // }
        }
        //load from db
        else {
            $events = $this->eventStore->getEvents($aggregateId);
            if(empty($events)) {
                return null;
            }
            $aggregateRoot = call_user_func($class.'::fromEvents', $events);

            //cache result
            $this->roots[$aggregateId] = $aggregateRoot;
        }
        return $aggregateRoot;
    }

    /**
     * @param $class
     * @param string $aggregateId
     * @param int $version
     * @return AggregateRoot
     * @throws AggregateException
     */
    public function loadAggregateRootToVersion($class, string $aggregateId, int $version): AggregateRoot
    {
        if(!$this->isAggregateRootClass($class)){
            throw new AggregateException('You cannot load non AggregateRoot object');
        }

        $events = $this->eventStore->getEventsToVersion($aggregateId, $version);
        if(empty($events)) {
            return null;
        }
        return call_user_func($class.'::fromEvents', $events);
    }

    public function isAggregateRootClass($class) : bool
    {
        try {
            $reflection = new ReflectionClass($class);
            return $reflection->isSubclassOf(AggregateRoot::class);
        }
        catch (ReflectionException $e) {
            return false;
        }
    }
}
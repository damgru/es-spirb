<?php


namespace App\Controller;

use App\Common\CommandBus;
use App\Common\ReadModel\ReadModelManager;
use App\Common\Uuid;
use App\Planning\Command\ActionCreate;
use App\Planning\Domain\Action\ActionType;
use App\Planning\AppService\ActionAppService;
use Exception;
use Money\Money;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TestController extends AbstractController
{
    /** @var CommandBus */
    private $commandBus;

    /**
     * TestController constructor.
     * @param CommandBus $commandBus
     */
    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    /**
     * @Route("/", name="start")
     */
    public function hello() {
        return $this->render('action/index.html.twig', [
            'asdf' => 'asdf'
        ]);
    }

    /**
     * @param $money
     * @return Response
     * @throws Exception
     * @Route("/action/create/{type}/{symbol}/{money}", name="action-create")
     */
    public function createAction($type, $symbol, $money) {

        $uuid = Uuid::generate();
        $amount = Money::PLN($money);

        $this->commandBus->handle(new ActionCreate($uuid, $amount, '', $type, $symbol));

        return $this->render('base.html.twig', [
            'asdf' => $uuid->toString()
        ]);
    }

    /**
     * @param $money
     * @return Response
     * @throws Exception
     * @Route("/action/{uuid}/create/{type}/{symbol}/{money}", name="action-create-child")
     */
    public function createChildAction($uuid, $type, $symbol, $money) {

        $parentUuid = Uuid::fromString($uuid);
        $uuid = Uuid::generate();
        $amount = Money::PLN($money);

        $this->commandBus->handle(new ActionCreate($uuid, $amount, $parentUuid, $type, $symbol));

        return $this->render('base.html.twig', [
            'asdf' => $uuid->toString()
        ]);
    }
}
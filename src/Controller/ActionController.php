<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ActionController
{
    /**
     * @Route("/action", name="action")
     */
    public function index()
    {
        return new Response('asdf');
    }
}

<?php

final class MyAggEvent
{
    protected string $name;
    protected string $aggId;
    protected array $payload = [];

    public function __construct(string $name, string $aggId, array $payload = [])
    {
        $this->name = $name;
        $this->aggId = $aggId;
        $this->payload = $payload;
    }

    public function payload(): array
    {
        return $this->payload;
    }

    public function aggId(): string
    {
        return $this->aggId;
    }

    public function name(): string
    {
        return $this->name;
    }
}

final class MyAgg
{
    /** @var MyAggEvent[] */
    private array $changes = [];

    private string $aggId;
    private int $somethingHappenedTimes = 0;
    private int $sumOfA = 0;

    public static function create(string $aggId): MyAgg
    {
        $myAgg = new self();
        $myAgg->record(new MyAggEvent('created', $aggId));
        return $myAgg;
    }

    public function doSomething()
    {
        $this->record(new MyAggEvent('something-happened', $this->aggId, ['a' => '1']));
    }

    public function doSomethingElse()
    {
        //validate
        $this->record(new MyAggEvent('something-else-happened', $this->aggId, ['b' => '2']));
    }

    private function record(MyAggEvent $event)
    {
        $this->changes[] = $event;
        $this->apply($event);
    }

    public function apply(MyAggEvent $event)
    {
        if ($event->name() === 'created') {
            $this->aggId = $event->aggId();
        }
        elseif ($event->name() === 'something-happened') {
            $this->somethingHappenedTimes++;
            $this->sumOfA += $event->payload()['a'];
        }
        elseif ($event->name() === 'something-else-happened') {
            $this->somethingHappenedTimes++;
        }
    }

    public function getChanges() : array
    {
        return $this->changes;
    }

    public function clearChanges()
    {
        $this->changes = [];
    }
}

interface EventStore
{
    public function addEvent(string $stream, string $aggId, string $eventName, array $payload);
    public function getEventsByAggId(string $aggId): iterable;
}

final class MyAggRepo
{
    private EventStore $store;

    /**
     * MyAggRepo constructor.
     * @param EventStore $store
     */
    public function __construct(EventStore $store)
    {
        $this->store = $store;
    }

    public function save(MyAgg $agg)
    {
        foreach ($agg->getChanges() as $change)
        {
            $this->store->addEvent('my_agg', $change->aggId(), $change->name(), $change->payload());
        }
        $agg->clearChanges();
    }

    public function load(string $aggId) : MyAgg
    {
        $myAgg = new MyAgg();
        foreach ($this->store->getEventsByAggId($aggId) as $event)
        {
            $myAgg->apply(new MyAggEvent($event['name'], $event['agg_id'], $event['payload']));
        };
        return $myAgg;
    }
}

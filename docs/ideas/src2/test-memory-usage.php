<?php

class PlanNode {
    /** @var \App\Common\Amount */
    public $plan;
    /** @var string */
    public $symbol;

    /** @var PlanNode[] */
    public $childs;
}

class Plan {
    /** @var PlanNode */
    public $earnings;
    /** @var PlanNode */
    public $expenses;
}

echo ((memory_get_usage()/1024.0)/1024.0)."\n";
$plan = new Plan();
$plan->expenses = new PlanNode();
$plan->earnings = new PlanNode();
for ($i=100;$i<1000; $i++) {
    $dzial = new PlanNode();
    $dzial->symbol = $i;
    $dzial->plan = 100000;
    for ($j=0;$j<100; $j++) {
        $rozdzial = new PlanNode();
        $rozdzial->plan = 1000;
        for ($k=0;$k<15; $k++) {
            $paragraf = new PlanNode();
            $paragraf->plan = 65;
            $rozdzial->childs[] = $paragraf;
        }
        $dzial->childs[] = $rozdzial;
    }
    $plan->expenses->childs[] = $dzial;
    $plan->earnings->childs[] = $dzial;
}
echo ((memory_get_usage()/1024.0)/1024.0)."\n";

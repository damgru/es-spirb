#Action
- create
- increasePlan(money)
- decreasePlan(money)
- engage(money)
- unengage(money)

+ ActionCreated
+ PlanIncreased
+ PlanDecreased
+ PlanUsed
+ PlanReturned
+ MoneyEngaged
+ MoneyUnengaged

+ PrognoseClosed
+ PropositionClosed
+ ProjectClosed
+ OutgoingClosed
+ ChangesClosed
